package com.cardence.lawshelf;

import static org.junit.Assert.*;

import org.junit.Test;

import com.cardence.lawshelf.model.helper.SequenceTranslator;

public class SequenceTranslatorTest {

	@Test
	public void testTranslateLowerCaseLetterToInt() {
		
		SequenceTranslator sut = new SequenceTranslator();

		assertEquals(1, sut.translateLowerCaseLetterToInt('a'));
		assertEquals(2, sut.translateLowerCaseLetterToInt('b'));
		assertEquals(26, sut.translateLowerCaseLetterToInt('z'));
		assertEquals(0, sut.translateLowerCaseLetterToInt('B'));
		assertEquals(0, sut.translateLowerCaseLetterToInt('C'));
		assertEquals(0, sut.translateLowerCaseLetterToInt('D'));
		assertEquals(0, sut.translateLowerCaseLetterToInt('Z'));
		
	}

	@Test
	public void testCalculateFinalBase26Value_SingleValue() {
		
		SequenceTranslator sut = new SequenceTranslator();

		
		//A
		assertEquals(1, sut.calculateFinalBase26Value(new Integer[] {1}));
		
		//Z
		assertEquals(26, sut.calculateFinalBase26Value(new Integer[] {26}));
		
	}
	@Test
	public void testCalculateFinalBase26Value_DoubleValue() {
		
		SequenceTranslator sut = new SequenceTranslator();

		// BC
		assertEquals(55, sut.calculateFinalBase26Value(new Integer[] {3,2}));
		
		//AA
		assertEquals(27, sut.calculateFinalBase26Value(new Integer[] {1,1}));
		
	}

	@Test
	public void testTranslateLetterToSequence_SingleValue() {
		
		SequenceTranslator sut = new SequenceTranslator();

		assertEquals(1, sut.translateLetterToSequence("a"));

		assertEquals(1, sut.translateLetterToSequence("A"));

		assertEquals(26, sut.translateLetterToSequence("z"));

		assertEquals(26, sut.translateLetterToSequence("Z"));
		
		
	}
	@Test
	public void testTranslateLetterToSequence_DoubleValue() {
		
		SequenceTranslator sut = new SequenceTranslator();

		assertEquals(27, sut.translateLetterToSequence("aa"));
		assertEquals(27, sut.translateLetterToSequence("Aa"));
		assertEquals(27, sut.translateLetterToSequence("AA"));

		assertEquals(55, sut.translateLetterToSequence("bc"));
		assertEquals(55, sut.translateLetterToSequence("Bc"));
		assertEquals(55, sut.translateLetterToSequence("BC"));
		
		
	}

}
