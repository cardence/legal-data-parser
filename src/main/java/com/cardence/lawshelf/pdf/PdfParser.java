package com.cardence.lawshelf.pdf;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import com.cardence.lawshelf.html.HtmlParserHandler;

public interface PdfParser {

	//void setParserHandler(HtmlParserHandler handler);

	void parseFile(String filepath) throws IOException;

	void parseFile(File file) throws IOException;

	void parseUrl(URL url) throws IOException;

	//void parseHtmlString(String html);

	void processStructure();

}
