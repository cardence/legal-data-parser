package com.cardence.lawshelf;

public interface UserInteraction {

	public abstract String askQuestion(String question,
			UserChoicePresenter choices);

}