package com.cardence.lawshelf.model;

public interface ContentFullDao {

	ContentFull findContentFullBySection(int sectionId);

	ContentFull findContentFull(int id);
	
	void updateContentFull(ContentFull content);

	int createContentFull(ContentFull content);

	void deleteAllForSection(int sectionId);

	void deleteAllForCode(Integer codeId);
}
