package com.cardence.lawshelf.model;

import java.util.Collection;

public interface SectionDao {

	Section findSection(int id);

	Section findSectionByCodeAndReference(int codeId, String ref);

	Section findSectionByLevelTypeAndPosition(String levelType,
			String levelPosition);

	Collection<Section> findSectionsByParent(int parentId);

	Collection<Section> findSectionByReference(String ref);

	Collection<Section> findTopLevelSectionByCode(int codeId);

	Collection<Section> findSectionByCode(int codeId);

	void updateSection(Section section);

	int createSection(Section section);

	void deleteAllForCode(Integer codeId);
}
