package com.cardence.lawshelf.model;

public class UscPrelimCollection extends UscCollection {

	public static final int USC_PRELIM_YEAR_PLACEHOLDER = 9999;

	@Override
	public Integer getYear() {
		return USC_PRELIM_YEAR_PLACEHOLDER;
	}

	@Override
	public boolean isFederal() {
		return true;
	}

	@Override
	public boolean isState() {
		return false;
	}

	@Override
	public String getStateCode() {
		return null;
	}

	@Override
	public String getName() {
		return USC_TITLE;
	}

	@Override
	public String getHeading() {
		return USC_ABBR + " (prelim)";
	}

	@Override
	protected String getSourceOfData() {
		return "USC";
	}
}
