package com.cardence.lawshelf.model;

public class ContentPart extends BaseDatabaseObject {

	private StringBuffer content;
	private boolean isHeader;
	private boolean isNotes;

	private String contentType;
	private String notesType;
	private String formatType;
	private int contentSequence;

	private ContentFull contentFull;
	private Section section;
	private Integer sectionId;
	private Integer contentId;

	private StringBuffer getContentVar() {
		if (this.content == null) {
			this.content = new StringBuffer();
		}
		return this.content;
	}

	public String getContent() {
		return getContentVar().toString();
	}

	public void setContent(String content) {
		this.content = null;
		this.addContent(content);
	}

	public void addContent(String content) {
		getContentVar().append(content);
	}

	public boolean isNotes() {
		return isNotes;
	}

	public void setNotes(boolean isNotes) {
		this.isNotes = isNotes;
	}

	public boolean isHeader() {
		return isHeader;
	}

	public void setHeader(boolean isHeader) {
		this.isHeader = isHeader;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Integer getSectionId() {
		return (getSection() == null) ? sectionId : getSection().getId();
	}

	public void setSectionId(Integer id) {
		this.sectionId = id;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public String getNotesType() {
		return notesType;
	}

	public void setNotesType(String notesType) {
		this.notesType = notesType;
	}

	public int getContentSequence() {
		return contentSequence;
	}

	public void setContentSequence(int contentSequence) {
		this.contentSequence = contentSequence;
	}

	@Override
	protected String getAttributeType() {
		return "CONTENT_PART";
	}

	public ContentFull getContentFull() {
		return contentFull;
	}

	public void setContentFull(ContentFull contentFull) {
		this.contentFull = contentFull;
	}

	public Integer getContentId() {
		return (getContentFull() == null) ? contentId : getContentFull()
				.getId();
	}

	public void setContentId(Integer id) {
		this.contentId = id;
	}

	public String getFormatType() {
		return formatType;
	}

	public void setFormatType(String formatType) {
		this.formatType = formatType;
	}

	@Override
	public String toString() {
		return "ContentPart [content=" + getContent() + ", isHeader="
				+ isHeader() + ", isNotes=" + isNotes() + ", contentType="
				+ getContentType() + ", notesType=" + getNotesType()
				+ ", formatType=" + getFormatType() + ", contentSequence="
				+ getContentSequence() + ", contentId=" + getContentId()
				+ ", sectionId=" + getSectionId() + ", toString()="
				+ super.toString() + "]";
	}

}
