package com.cardence.lawshelf.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class BaseDatabaseObject {

	private Set<Attribute> attributeSet;
	private HashMap<String, Attribute> rawAttributeMap;

	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
		validateAttributeSet();
	}

	public void addAttribute(String key, String value) {
		addAttribute(getAttributeType(), this.id, key, value, getSourceOfData());
	}

	protected abstract String getAttributeType();

	private Map<String, Attribute> getRawAttributeMap() {
		if (this.rawAttributeMap == null) {
			this.rawAttributeMap = new HashMap<String, Attribute>();
		}
		return this.rawAttributeMap;
	}

	public String getRawSourceAttributeValue(String key) {
		Attribute attr = getRawAttributeMap().get(key);
		return (attr == null) ? null : attr.getAttrValue();
	}

	public Attribute getRawSourceAttribute(String key) {
		return getRawAttributeMap().get(key);
	}

	public Collection<Attribute> getAttributes() {
		return Collections.unmodifiableCollection(getAttributeSet());
	}

	private Set<Attribute> getAttributeSet() {
		if (this.attributeSet == null) {
			this.attributeSet = new HashSet<Attribute>();
		}
		return this.attributeSet;
	}

	private void addAttribute(String tabname, Integer tabId, String key,
			String val, String source) {
		Attribute attr = new Attribute();
		attr.setAttrKey(key);
		attr.setAttrValue(val);
		attr.setTablename(tabname);
		attr.setTableID(tabId);
		attr.setSource(source);
		getAttributeSet().add(attr);
		getRawAttributeMap().put(key, attr);
	}

	private void validateAttributeSet() {
		synchronized (this) {
			if (getId() == null) {
				return;
			}
			for (Attribute attr : getAttributeSet()) {
				if (attr.getTableID() == null) {
					attr.setTableID(getId());
				}
			}
		}
	}

	protected String getSourceOfData() {
		return "";
	}

	@Override
	public String toString() {
		return "BaseDatabaseObject [id=" + getId() + ", attributeSet="
				+ attributeSet + ", rawAttributeMap=" + rawAttributeMap + "]";
	}
}
