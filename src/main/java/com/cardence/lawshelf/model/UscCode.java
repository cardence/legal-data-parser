package com.cardence.lawshelf.model;

public class UscCode extends Code {

	/* TITLE # - XXXXXX */
	/* # U.S.C. �### */
	public static final String USC_HEADING = "TITLE";
	public static final String USC_SHORT_HEADING = "U.S.C.";

	public UscCode() {
		super();
	}

	@Override
	public String getTitle() {
		return USC_HEADING + " " + getCodeSequence();
	}

	@Override
	public String getHeading() {
		return getTitle() + " - " + getName();
	}

	@Override
	public String getShortHeading() {
		return getCodeSequence() + " " + USC_SHORT_HEADING;
	}

	@Override
	protected String getSourceOfData() {
		return "USC";
	}
}
