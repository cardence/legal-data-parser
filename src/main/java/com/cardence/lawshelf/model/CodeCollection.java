package com.cardence.lawshelf.model;

public class CodeCollection extends BaseDatabaseObject {

	private Integer year;
	private String heading;
	private String name;
	private boolean isFederal;
	private String stateCode;

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public boolean isFederal() {
		return isFederal;
	}

	public void setFederal(boolean isFederal) {
		this.isFederal = isFederal;
	}

	public boolean isState() {
		return (this.stateCode != null);
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected String getAttributeType() {
		return "CODECOLLECTION";
	}

	@Override
	public String toString() {
		return "CodeCollection [year=" + getYear() + ", heading="
				+ getHeading() + ", name=" + getName() + ", isFederal="
				+ isFederal() + ", stateCode=" + getStateCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
