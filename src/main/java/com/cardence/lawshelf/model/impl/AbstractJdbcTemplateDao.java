package com.cardence.lawshelf.model.impl;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.cardence.lawshelf.model.DataSourceDao;

public abstract class AbstractJdbcTemplateDao implements DataSourceDao {

	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	protected JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}

	public void deleteAllRecords() {
		try {
			this.getJdbcTemplate().execute(
					"delete from " + getDatabaseTableName());
		} catch (Throwable t) {
			System.out.println("Did not delete records from table ["
					+ getDatabaseTableName() + "]: " + t.getLocalizedMessage());
		}
	}

	public void deleteRecord(Integer id) {
		try {
			this.getJdbcTemplate().update(
					"delete from " + getDatabaseTableName() + " where id = ?",
					new Object[] { //
					id }, new int[] { java.sql.Types.INTEGER });
		} catch (Throwable t) {
			System.out.println("Did not delete records from table ["
					+ getDatabaseTableName() + "] with id [" + id + "]: "
					+ t.getLocalizedMessage());
		}
	}

	public void deleteRecord(String colname, Integer id) {
		try {
			this.getJdbcTemplate().update(
					"delete from " + getDatabaseTableName() + " where "
							+ colname + " = ?", new Object[] { //
					id }, new int[] { java.sql.Types.INTEGER });
		} catch (Throwable t) {
			System.out.println("Did not delete record from table ["
					+ getDatabaseTableName() + "] for column [" + colname
					+ "] with value [" + id + "]: " + t.getLocalizedMessage());
		}
	}

	protected abstract String getDatabaseTableName();
}
