package com.cardence.lawshelf.model.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

import com.cardence.lawshelf.model.CodeAlias;
import com.cardence.lawshelf.model.CodeAliasDao;

public class JdbcCodeAliasDao extends AbstractJdbcTemplateDao implements
		CodeAliasDao {

	public static final String TABLE_NAME = "code_alias";
	private static final String SQL_FIND = "select code_id, aliasname from code_alias";
	private static final String SQL_INSERT = "insert into code_alias (code_id, aliasname) values (?,?)";

	// private static final String SQL_DELETE_ATTR =
	// "delete from attribute where tablename = ? and tablerow_id = ?";

	private static final class AliasMapper implements RowMapper<CodeAlias> {
		public CodeAlias mapRow(ResultSet rs, int rowNum) throws SQLException {
			CodeAlias alias = new CodeAlias();
			alias.setAliasname(rs.getString("aliasname"));
			alias.setCodeId(rs.getInt("code_id"));

			return alias;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cardence.lawshelf.model.impl.AttributeDao#createAttribute(com.cardence
	 * .lawshelf.model.Attribute)
	 */
	public void createAlias(CodeAlias alias) {

		try {
			final String name = alias.getAliasname();
			final int codeId = alias.getCodeId();

			this.getJdbcTemplate().update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(
						Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(
							SQL_INSERT, new String[] {});

					ps.setInt(1, codeId);
					ps.setString(2, name);

					return ps;
				}
			});
		} catch (Throwable t) {
			System.out
					.println("Did not create code alias... possibly already there: "
							+ t.getLocalizedMessage());
		}

	}

	@Override
	protected String getDatabaseTableName() {
		return TABLE_NAME;
	}

	public void deleteAllForCode(Integer codeId) {
		this.deleteRecord("code_id", codeId);
	}
}
