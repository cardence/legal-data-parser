package com.cardence.lawshelf.model.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

import com.cardence.lawshelf.model.Attribute;
import com.cardence.lawshelf.model.AttributeDao;

public class AttributeEnabledJdbcTemplateDao extends AbstractJdbcTemplateDao
		implements AttributeDao {

	private static final String TABLE_NAME = "attribute";
	private static final String SQL_FIND_ATTR = "select tablename, tablerow_id, attr_key, attr_value, source, from attribute";
	private static final String SQL_INSERT_ATTR = "insert into attribute (tablename, tablerow_id, attr_key, attr_value, source) values (?,?,?,?,?)";

	// private static final String SQL_DELETE_ATTR =
	// "delete from attribute where tablename = ? and tablerow_id = ?";

	private static final class AttributeMapper implements RowMapper<Attribute> {
		public Attribute mapRow(ResultSet rs, int rowNum) throws SQLException {
			Attribute attr = new Attribute();
			attr.setAttrKey(rs.getString("attr_key"));
			attr.setAttrValue(rs.getString("attr_value"));
			attr.setTableID(rs.getInt("tablerow_id"));
			attr.setTablename(rs.getString("tablename"));
			attr.setSource(rs.getString("source"));

			return attr;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cardence.lawshelf.model.impl.AttributeDao#findAttributesForRow(java
	 * .lang.String, int)
	 */
	public Collection<Attribute> findAttributesForRow(String tablename,
			int tablerowid) {
		return this.getJdbcTemplate().query(
				SQL_FIND_ATTR + " where tablename = ? and tablerow_id = ?",
				new Object[] { tablename, tablerowid },
				new int[] { java.sql.Types.VARCHAR, java.sql.Types.INTEGER },
				new AttributeMapper());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cardence.lawshelf.model.impl.AttributeDao#findAttributeForRow(java
	 * .lang.String, int, java.lang.String)
	 */
	public Attribute findAttributeForRow(String tablename, int tablerowid,
			String attributeKey) {

		return (Attribute) this
				.getJdbcTemplate()
				.queryForObject(
						SQL_FIND_ATTR
								+ " where tablename = ? and tablerow_id = ? and attr_key = ?",
						new Object[] { tablename, tablerowid, attributeKey },
						new AttributeMapper());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cardence.lawshelf.model.impl.AttributeDao#deleteAttribute(java.lang
	 * .String, java.lang.Integer)
	 */
	public void deleteAttribute(String tablename, Integer tablerowId) {
		try {
			this.getJdbcTemplate()
					.update("delete from " + TABLE_NAME
							+ " where tablename = ? and tablerow_id = ?",
							new Object[] { //
							tablename, tablerowId },
							new int[] { java.sql.Types.VARCHAR,
									java.sql.Types.INTEGER });
		} catch (Throwable t) {
			System.out
					.println("Did not delete attribute... possibly not there: "
							+ t.getLocalizedMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cardence.lawshelf.model.impl.AttributeDao#createAttribute(com.cardence
	 * .lawshelf.model.Attribute)
	 */
	public void createAttribute(Attribute attr) {
		try {
			final String tablename = attr.getTablename();
			final Integer tablerowid = attr.getTableID();
			final String attrKey = attr.getAttrKey();
			final String attrVal = attr.getAttrValue();
			final String source = attr.getSource();

			this.getJdbcTemplate().update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(
						Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(
							SQL_INSERT_ATTR, new String[] {});
					ps.setString(1, tablename);
					ps.setInt(2, tablerowid);
					ps.setString(3, attrKey);
					ps.setString(4, attrVal);
					ps.setString(5, source);
					return ps;
				}
			});
		} catch (Throwable t) {
			System.out
					.println("Did not create attribute... possibly already there: "
							+ t.getLocalizedMessage());
		}
	}

	public void replaceAllAttributes(Collection<Attribute> attrs) {
		Map<String, Object[]> tableIdComboMap = new HashMap<String, Object[]>();

		// // group all the unique table and row id combinations
		for (Attribute attr : attrs) {
			String tn = attr.getTablename();
			Integer tid = attr.getTableID();
			if (tn != null && tid != null) {
				tableIdComboMap.put(tn + "-" + tid.toString(), new Object[] {
						tn, tid });
			}
		}

		// // delete all attributes for this table/id combo
		for (Object[] tableIdCombo : tableIdComboMap.values()) {
			this.deleteAttribute((String) tableIdCombo[0],
					(Integer) tableIdCombo[1]);
		}

		// // now create all the new attributes
		for (Attribute attr : attrs) {
			this.createAttribute(attr);
		}
	}

	public void createAllAttributes(Collection<Attribute> attrs) {
		// // now create all the new attributes
		for (Attribute attr : attrs) {
			this.createAttribute(attr);
		}
	}

	@Override
	protected String getDatabaseTableName() {
		return TABLE_NAME;
	}

}
