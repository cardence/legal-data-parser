package com.cardence.lawshelf.model.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.cardence.lawshelf.model.ContentFull;
import com.cardence.lawshelf.model.ContentFullDao;
import com.cardence.lawshelf.model.Section;

public class JdbcContentFullDao extends AttributeEnabledJdbcTemplateDao
		implements ContentFullDao {

	public static final String TABLE_NAME = "content_full";
	private static final String SQL_FIND = "select id, section_id, content, format_type, is_only_notes from content_full";
	private static final String SQL_UPDATE = "update content_full set (content, format_type, is_only_notes) = (?,?,?) where id = ?";
	private static final String SQL_CREATE = "insert into content_full (section_id, content, format_type, is_only_notes) values (?,?,?,?)";

	private static final class ContentMapper implements RowMapper<ContentFull> {
		public ContentFull mapRow(ResultSet rs, int rowNum) throws SQLException {
			ContentFull c = new ContentFull();
			Section section = new Section();
			section.setId(rs.getInt("section_id"));
			c.setContent(rs.getString("content"));
			c.setSection(section);
			c.setSectionId(section.getId());
			c.setNotes(rs.getBoolean("is_only_notes"));
			c.setFormatType(rs.getString("format_type"));
			c.setId(rs.getInt("id"));
			return c;
		}
	}

	public ContentFull findContentFullBySection(int sectionId) {
		try {
			return (ContentFull) this.getJdbcTemplate().queryForObject(
					SQL_FIND + " where section_id = ?",
					new Object[] { sectionId }, new ContentMapper());
		} catch (Throwable t) {
			System.out
					.println("No Results for Query (findContentFullBySection) with sectionId ["
							+ sectionId + "]");
			return null;
		}
	}

	public ContentFull findContentFull(int id) {
		return (ContentFull) this.getJdbcTemplate().queryForObject(
				SQL_FIND + " where id = ?", new Object[] { id },
				new ContentMapper());
	}

	public void updateContentFull(ContentFull content) {
		this.getJdbcTemplate().update(SQL_UPDATE, new Object[] { //
				content.getContent(),//
						content.getFormatType(),//
						content.isNotes(),//
						content.getId() //
				}, new int[] { //
				java.sql.Types.VARCHAR,//
						java.sql.Types.VARCHAR,//
						java.sql.Types.BOOLEAN,//
						java.sql.Types.INTEGER //
				});
	}

	public int createContentFull(ContentFull content) {
		try {

			final int sid = content.getSectionId();
			final String data = content.getContent();
			final String format = content.getFormatType();
			final boolean isNotes = content.isNotes();

			KeyHolder keyHolder = new GeneratedKeyHolder();
			this.getJdbcTemplate().update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(
						Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(
							SQL_CREATE, new String[] { "id" });
					ps.setInt(1, sid);
					ps.setString(2, data);
					ps.setString(3, format);
					ps.setBoolean(4, isNotes);
					return ps;
				}
			}, keyHolder);

			int newID = keyHolder.getKey().intValue();
			content.setId(newID);
			return newID;
		} catch (DataIntegrityViolationException e) {
			System.out.println("Could not create content full: "
					+ e.getLocalizedMessage());
			System.out.println(content);
			throw e;
		}
	}

	@Override
	protected String getDatabaseTableName() {
		return TABLE_NAME;
	}

	public void deleteAllForSection(int sectionId) {
		deleteRecord("section_id", sectionId);
	}

	public void deleteAllForCode(Integer codeId) {
		try {
			this.getJdbcTemplate().update(
					"delete from " + TABLE_NAME + " c join "
							+ JdbcSectionDao.TABLE_NAME
							+ " s on s.id = c.section_id where s.code_id = ?",
					new Object[] { //
					codeId }, new int[] { java.sql.Types.INTEGER });
		} catch (Throwable t) {
			System.out
					.println("Did not delete content full... possibly not there: "
							+ t.getLocalizedMessage());
		}
	}
}
