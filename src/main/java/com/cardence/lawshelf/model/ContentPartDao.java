package com.cardence.lawshelf.model;

import java.util.Collection;

public interface ContentPartDao {

	ContentPart findContentPart(int id);

	Collection<ContentPart> findContentPartForSection(int sectionId);

	void updateContentPart(ContentPart content);

	int createContentPart(ContentPart content);
	
	void deleteAllForSection(int sectionId);

	void deleteAllForCode(Integer codeId);
	
	void deleteAllAttributesForSection(int sectionId);
}
