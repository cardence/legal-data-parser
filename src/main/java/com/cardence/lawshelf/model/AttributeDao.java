package com.cardence.lawshelf.model;

import java.util.Collection;

public interface AttributeDao {

	public abstract Collection<Attribute> findAttributesForRow(
			String tablename, int tablerowid);

	public abstract Attribute findAttributeForRow(String tablename,
			int tablerowid, String attributeKey);

	public abstract void deleteAttribute(String tablename, Integer tablerowId);

	public abstract void createAttribute(Attribute attr);

	public abstract void replaceAllAttributes(Collection<Attribute> attrs);

	public abstract void createAllAttributes(Collection<Attribute> attrs);

}