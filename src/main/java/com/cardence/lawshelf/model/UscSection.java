package com.cardence.lawshelf.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.util.StringUtils;

import com.cardence.lawshelf.handler.UscTags;
import com.cardence.lawshelf.model.helper.RomanNumeralUtils;

public class UscSection extends Section {

	protected String getSourceOfData() {
		return "USC";
	}

	@Override
	public String getShortHeading() {
		ArrayList<String> delimList = new ArrayList<String>();
		if (isBottomLevelSection() || isBottomLevelRule()) {
			// look for '. '
			delimList.add(". ");
			delimList.add(".\u00A0");
			delimList.add(".&nbsp;");
		} else {
			// look for &mdash;
			// in java, this is \u2014
			delimList.add("\u2014");
			delimList.add("&mdash;");
		}

		String headingString = getHeading();

		for (Iterator<String> it = delimList.iterator(); it.hasNext();) {
			String delim = it.next();
			String[] headingParts = StringUtils.split(headingString, delim);
			if (headingParts != null) {
				String s = StringUtils.replace(headingParts[0], "[", "");
				return StringUtils.trimWhitespace(s);
			}
		}

		// if we are here, we did not match.
		// hmm... let's see if we can match the section number
		if (isBottomLevelSection()) {
			String[] headingParts = StringUtils.split(headingString,
					findBottomLevelSectionNumber());
			if (headingParts != null) {
				String s = StringUtils.replace(headingParts[0], "[", "");
				return StringUtils.trimWhitespace(s);
			} else {

			}
		}

		if (headingString != null && headingString.length() > 150) {
			headingString = org.apache.commons.lang.StringUtils.substring(
					headingString, 0, 145);
			headingString = org.apache.commons.lang.StringUtils
					.substringBeforeLast(headingString, " ");
			headingString += "...";
		}
		return headingString;
	}

	@Override
	public String getTextHeading() {
		ArrayList<String> delimList = new ArrayList<String>();
		if (isBottomLevelSection() || isBottomLevelRule()) {
			// look for '. '
			delimList.add(". ");
			delimList.add(".\u00A0");
			delimList.add(".&nbsp;");
		} else {
			// look for &mdash;
			// in java, this is \u2014
			delimList.add("\u2014");
			delimList.add("&mdash;");
		}

		String headingString = getHeading();

		for (Iterator<String> it = delimList.iterator(); it.hasNext();) {
			String delim = it.next();
			String[] headingParts = StringUtils.split(headingString, delim);
			if (headingParts != null) {
				String s = headingParts[1];
				s = StringUtils.replace(s, "[", "");
				s = StringUtils.replace(s, "]", "");
				return StringUtils.trimWhitespace(s);
			}
		}

		// if we are here, we did not match.
		// hmm... let's see if we can match the section number
		if (isBottomLevelSection()) {
			String[] headingParts = StringUtils.split(headingString,
					findBottomLevelSectionNumber());
			if (headingParts != null) {
				String s = headingParts[1];
				s = StringUtils.replace(s, "[", "");
				s = StringUtils.replace(s, "]", "");
				return StringUtils.trimWhitespace(s);
			} else {

			}
		}

		if (headingString != null && headingString.length() > 150) {
			headingString = org.apache.commons.lang.StringUtils.substring(
					headingString, 0, 145);
			headingString = org.apache.commons.lang.StringUtils
					.substringBeforeLast(headingString, " ");
			headingString += "...";
		}
		return headingString;
	}

	private boolean isBottomLevelSection() {
		return (UscTags.LEVEL_TYPE_SECTION.equals(this.getLevelType()));
	}

	private boolean isBottomLevelRule() {
		return (UscTags.LEVEL_TYPE_RULE.equals(this.getLevelType()));
	}

	private boolean isRomanNumeralLevelPosition() {
		return (UscTags.LEVEL_TYPE_WITH_ROMAN_NUMERALS.contains(this
				.getLevelType()));
	}

	@Override
	public String getSourceReference() {
		return this.getRawSourceAttributeValue("itempath");
	}

	@Override
	public String getLevelType() {
		if (super.getLevelType() == null) {
			String itempath = this.getSourceReference();

			// get the primary key for the "parent" section
			String[] pathelements = StringUtils.tokenizeToStringArray(itempath,
					"/", true, true);
			if (pathelements != null) {
				if (pathelements.length == 1) {
					super.setLevelType(UscTags.LEVEL_TYPE_TITLE);
				} else if (pathelements.length > 1) {
					String targetElement = pathelements[pathelements.length - 1];
					String[] targetSplit = StringUtils.tokenizeToStringArray(
							targetElement, " ", true, true);

					if (targetSplit != null && targetSplit.length > 0) {
						super.setLevelType(normalizeLevelType(targetSplit[0]));
					}
				}
			}
		}
		return super.getLevelType();
	}

	@Override
	public String getLevelPosition() {
		if (super.getLevelPosition() == null) {
			String parentLevel = getParentLevelPosition();
			if (parentLevel == null) {
				super.setLevelPosition("" + getSectionSequence());
			} else {
				super.setLevelPosition(getParentLevelPosition() + "."
						+ getSectionSequence());
			}
		}

		return super.getLevelPosition();
	}

	/**
	 * @deprecated
	 */
	public String getLevelPositionOld() {
		if (super.getLevelPosition() == null) {

			String itempath = this.getSourceReference();

			// get the primary key for the "parent" section
			String[] pathelements = StringUtils.tokenizeToStringArray(itempath,
					"/", true, true);
			if (pathelements != null) {
				if (pathelements.length == 1) {
					if (getCode() != null) {
						super.setLevelPosition(getCode().getCodeSequence() + "");
					}
				} else if (pathelements.length > 1) {
					StringBuffer sb = new StringBuffer();

					if (getCode() != null) {
						sb.append(getCode().getCodeSequence());
					} else {
						sb.append(pathelements[0]);
					}

					for (int i = 1; i < pathelements.length; i++) {
						String targetElement = pathelements[i];
						String[] targetSplit = StringUtils
								.tokenizeToStringArray(targetElement, " ",
										true, true);

						if (targetSplit != null && targetSplit.length > 0) {
							String typeValue = normalizeLevelType(targetSplit[0]);
							String pos = null;
							if (targetSplit.length == 1) {
								pos = StringUtils.trimAllWhitespace(typeValue);
							} else {
								pos = targetSplit[1];
							}
							if (pos != null) {
								sb.append(".");

								pos = StringUtils.trimWhitespace(pos
										.toUpperCase());
								int minLength = 5;
								if (UscTags.LEVEL_TYPE_WITH_ROMAN_NUMERALS
										.contains(typeValue)
										&& RomanNumeralUtils
												.looksLikeRomanNumeral(pos)) {
									// appears to be a roman numeral
									Integer romanNumeralIntegerValue = RomanNumeralUtils
											.convertToNumber(pos);
									if (romanNumeralIntegerValue != null) {
										pos = romanNumeralIntegerValue
												.toString();
										minLength = 3;
									}
								}

								sb.append(getSortableNumericString(pos,
										minLength));

							}
						}
					}

					super.setLevelPosition(sb.toString());
				}
			}
		}
		return super.getLevelPosition();
	}

	private String findBottomLevelSectionNumber() {
		String itempath = this.getSourceReference();

		// get the primary key for the "parent" section
		String[] pathelements = StringUtils.tokenizeToStringArray(itempath,
				"/", true, true);
		if (pathelements != null) {
			String targetElement = pathelements[pathelements.length - 1];
			String[] targetSplit = StringUtils.tokenizeToStringArray(
					targetElement, " ", true, true);

			if (targetSplit != null && targetSplit.length > 0) {
				String typeValue = normalizeLevelType(targetSplit[0]);
				String pos = null;
				if (targetSplit.length == 1) {
					pos = StringUtils.trimAllWhitespace(typeValue);
				} else {
					pos = targetSplit[1];
				}
				if (pos != null) {
					pos = StringUtils.trimWhitespace(pos.toUpperCase());
				}

				return pos;
			}
		}

		return null;
	}

	private String normalizeLevelType(String rawValue) {
		String normalizedValue = null;
		if (rawValue != null) {
			normalizedValue = StringUtils
					.trimWhitespace(rawValue.toUpperCase());
			normalizedValue = StringUtils.replace(normalizedValue, "[", "");
			normalizedValue = StringUtils.replace(normalizedValue, "]", "");
			normalizedValue = StringUtils.replace(normalizedValue, "{", "");
			normalizedValue = StringUtils.replace(normalizedValue, "}", "");
			normalizedValue = StringUtils.replace(normalizedValue, "(", "");
			normalizedValue = StringUtils.replace(normalizedValue, ")", "");

			for (Iterator<String> it = UscTags.LEVEL_TYPE_SECTION_MATCH
					.iterator(); it.hasNext();) {
				String match = it.next();
				if (StringUtils.countOccurrencesOf(normalizedValue, match) > 0) {
					normalizedValue = UscTags.LEVEL_TYPE_SECTION;
					break;
				}
			}
		}
		return normalizedValue;
	}

	private String getSortableNumericString(String s, int minLength) {
		// must be smaller than the minimum and it must start with a digit
		if (s.length() < minLength && NumberUtils.isDigits(s.substring(0, 1))) {
			if (NumberUtils.isDigits(s.substring(s.length() - 1, s.length()))) {
				// basically, this is accounting for a trailing letter
				// such as 114a. by adding an additional zero the sorting should
				// work
				// for 114, 114a, and 115... which would convert to
				// 00114, 00114a, 00115
				minLength++;
			}
			char[] filler = new char[minLength - s.length()];
			Arrays.fill(filler, '0');
			return new String(filler).concat(s);
		}
		return s;
	}

	@Override
	public String toString() {
		return "UscSection [getSourceReference()=" + getSourceReference()
				+ ", getLevelType()=" + getLevelType() + ", getShortHeading()="
				+ getShortHeading() + ", getHeading()=" + getHeading()
				+ ", getLevelPosition()=" + getLevelPosition()
				+ ", getSectionSequence()=" + getSectionSequence()
				+ ", getCodeId()=" + getCodeId() + ", getParentSectionId()="
				+ getParentSectionId() + ", getId()=" + getId()
				+ ", getAttributes()=" + getAttributes() + "]";
	}

}
