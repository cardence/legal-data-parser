package com.cardence.lawshelf.model;

public class CodeAlias {

	private String aliasname;

	private Integer codeId; // FK
	private Code code;

	public String getAliasname() {
		return aliasname;
	}

	public void setAliasname(String aliasname) {
		this.aliasname = aliasname;
	}

	public Integer getCodeId() {
		return codeId;
	}

	public void setCodeId(Integer codeId) {
		this.codeId = codeId;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "CodeAlias [aliasname=" + aliasname + ", codeId=" + codeId
				+ ", code=" + code + "]";
	}

}
