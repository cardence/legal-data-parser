package com.cardence.lawshelf.model;

import java.util.Collection;

public interface CodeCollectionDao {

	CodeCollection findCodeCollection(int id);

	Collection<CodeCollection> findAllCodeCollections();

	Collection<CodeCollection> findCodeCollectionsByYear(Integer year);

	Collection<CodeCollection> findCodeCollectionsByNameAndYear(String name,
			Integer year);

	Collection<CodeCollection> findCodeCollectionsByState(String statecode);

	Collection<CodeCollection> findCodeCollectionsForFederal();

	void updateCodeCollection(CodeCollection codeCollection);

	int createCodeCollection(CodeCollection codeCollection);
}
