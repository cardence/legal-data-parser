package com.cardence.lawshelf.model;

import java.util.HashSet;
import java.util.Set;

public class Code extends BaseDatabaseObject {

	private String name;
	private String heading;
	private String shortHeading;
	private int codeSequence;
	private String status;
	private String title;

	private CodeCollection codeCollection;
	private Integer codeCollectionId;

	private Set<String> aliases;

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public int getCodeSequence() {
		return codeSequence;
	}

	public void setCodeSequence(int codeSequence) {
		this.codeSequence = codeSequence;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCodeCollectionId() {
		return (getCodeCollection() == null) ? codeCollectionId
				: getCodeCollection().getId();
	}

	public void setCodeCollectionId(Integer id) {
		this.codeCollectionId = id;
	}

	public CodeCollection getCodeCollection() {
		return codeCollection;
	}

	public void setCodeCollection(CodeCollection codeCollection) {
		this.codeCollection = codeCollection;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getAliases() {
		if (this.aliases == null) {
			this.aliases = new HashSet<String>();
		}
		return this.aliases;
	}

	public void setAliases(Set<String> aliases) {
		this.aliases = aliases;
	}

	public void addAlias(String alias) {
		this.getAliases().add(alias);
	}

	public boolean isAlias(String alias) {
		return this.getAliases().contains(alias);
	}

	public void setShortHeading(String shortheading) {
		this.shortHeading = shortheading;
	}

	public String getShortHeading() {
		return (this.shortHeading != null) ? this.shortHeading : getHeading();
	}

	@Override
	protected String getAttributeType() {
		return "CODE";
	}

	@Override
	public String toString() {
		return "Code [name=" + getName() + ", heading=" + getHeading()
				+ ", shortheading=" + getShortHeading() + ", title="
				+ getTitle() + ", codeSequence=" + getCodeSequence()
				+ ", status=" + getStatus() + ", codeCollectionId="
				+ getCodeCollectionId() + ", aliases=" + aliases
				+ ", toString()=" + super.toString() + "]";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
