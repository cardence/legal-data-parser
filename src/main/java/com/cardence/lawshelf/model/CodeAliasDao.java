package com.cardence.lawshelf.model;


public interface CodeAliasDao {

	void createAlias(CodeAlias alias);

	void deleteAllForCode(Integer codeId);
}