package com.cardence.lawshelf.model;

public class ContentFull extends BaseDatabaseObject {

	private StringBuffer content;
	private String formatType;
	private boolean isNotes;

	private Section section;
	private Integer sectionId;

	private StringBuffer getContentVar() {
		if (this.content == null) {
			this.content = new StringBuffer();
		}
		return this.content;
	}

	public String getContent() {
		return getContentVar().toString();
	}

	public void setContent(String content) {
		this.content = null;
		this.addContent(content);
	}

	public void addContent(String content) {
		getContentVar().append(content);
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public Integer getSectionId() {
		return (getSection() == null) ? sectionId : getSection().getId();
	}

	public void setSectionId(Integer id) {
		this.sectionId = id;
	}

	public String getFormatType() {
		return formatType;
	}

	public void setFormatType(String formatType) {
		this.formatType = formatType;
	}

	public boolean isNotes() {
		return isNotes;
	}

	public void setNotes(boolean isNotes) {
		this.isNotes = isNotes;
	}

	@Override
	protected String getAttributeType() {
		return "CONTENT";
	}

	@Override
	public String toString() {
		return "ContentFull [content=" + content + ", formatType=" + formatType
				+ ", isNotes=" + isNotes + ", sectionId=" + getSectionId()
				+ ", toString()=" + super.toString() + "]";
	}
}
