package com.cardence.lawshelf.model;

public class Attribute {

	private String tablename;
	private String attrKey;
	private String attrValue;
	private String source;

	private Integer tableID; // FK

	public String getTablename() {
		return tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

	public String getAttrKey() {
		return attrKey;
	}

	public void setAttrKey(String attrKey) {
		this.attrKey = attrKey;
	}

	public String getAttrValue() {
		return attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	public Integer getTableID() {
		return tableID;
	}

	public void setTableID(Integer tableID) {
		this.tableID = tableID;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return "Attribute [tablename=" + tablename + ", attrKey=" + attrKey
				+ ", attrValue=" + attrValue + ", source=" + source
				+ ", tableID=" + getTableID() + "]";
	}

}
