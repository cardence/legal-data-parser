package com.cardence.lawshelf.model.cfr;

import com.cardence.lawshelf.cfr.CfrCode;
import com.cardence.lawshelf.model.Code;

public class CfrCodeModel extends Code {

	/* Title # - XXXXXX */
	/* # CFR #.# */
	public static final String CFR_HEADING = "TITLE";
	public static final String CFR_SHORT_HEADING = "CFR";

	public CfrCodeModel(CfrCode code) {
		this.copyValues(code);
	}

	private void copyValues(CfrCode code) {
		this.setHeading(code.getHeading());
		this.setStatus(code.getStatus());
		this.setName(code.getName());
		this.setTitle(code.getTitle());
		this.setShortHeading(code.getShortHeading());
		this.setCodeSequence(code.getCodeSequence());
	}

	@Override
	public String getTitle() {
		return CFR_HEADING + " " + getCodeSequence();
	}

	@Override
	public String getHeading() {
		return getTitle() + " - " + getName();
	}

	@Override
	public String getShortHeading() {
		return getCodeSequence() + " " + CFR_SHORT_HEADING;
	}

	@Override
	protected String getSourceOfData() {
		return CFR_SHORT_HEADING;
	}
}
