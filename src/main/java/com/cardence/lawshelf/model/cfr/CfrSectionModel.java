package com.cardence.lawshelf.model.cfr;

import com.cardence.lawshelf.cfr.CfrSection;
import com.cardence.lawshelf.model.Section;

public class CfrSectionModel extends Section {

	public CfrSectionModel(CfrSection section) {
		this.copyValues(section);
	}

	private void copyValues(CfrSection section) {
		this.setHeading(section.getHeading());
		this.setLevelPosition(section.getCategoryLevel());
		this.setLevelType(section.getCategoryName());
		this.setSourceReference(section.getFullUniqueSourceReference());
		this.setShortHeading(section.getReference());
		this.setTextHeading(section.getTitleText());
		this.setSectionSequence(section.getSequence());
	}

	protected String getSourceOfData() {
		return "CFR";
	}

}
