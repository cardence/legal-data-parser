package com.cardence.lawshelf.model.cfr;

import com.cardence.lawshelf.model.CodeCollection;

public class CfrCollectionModel extends CodeCollection {

	public static final String CFR_TITLE = "Code of Federal Regulations";
	public static final String CFR_ABBR = "CFR ";

	public CfrCollectionModel() {
		super();

		super.setName(CFR_TITLE);
		super.setStateCode(null);
		super.setFederal(true);
	}

	@Override
	public String getHeading() {
		return CFR_ABBR + super.getYear();
	}

	@Override
	protected String getSourceOfData() {
		return "CFR";
	}
}
