package com.cardence.lawshelf.model.helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;

@SuppressWarnings("unchecked")
public class RomanNumeralUtils {

	private static Map<String, Integer> romanNumeralToDigitMap = new HashMap<String, Integer>();
	private static Map<Integer, String> digitToRomanNumeralMap = new HashMap<Integer, String>();

	private static Map<String, Integer> oneLessMap = new HashMap<String, Integer>();

	public static final String ROMAN_NUMERAL_DIGIT_ONE = "I";
	public static final String ROMAN_NUMERAL_DIGIT_FIVE = "V";
	public static final String ROMAN_NUMERAL_DIGIT_TEN = "X";
	public static final String ROMAN_NUMERAL_DIGIT_FIFTY = "L";
	public static final String ROMAN_NUMERAL_DIGIT_ONE_HUNDRED = "C";
	public static final String ROMAN_NUMERAL_DIGIT_FIVE_HUNDRED = "D";
	public static final String ROMAN_NUMERAL_DIGIT_ONE_THOUSAND = "M";

	private static final String[] ROMAN_NUMERAL_ORDER_DESCENDING = new String[] {
			ROMAN_NUMERAL_DIGIT_ONE_THOUSAND, //
			ROMAN_NUMERAL_DIGIT_FIVE_HUNDRED, //
			ROMAN_NUMERAL_DIGIT_ONE_HUNDRED, //
			ROMAN_NUMERAL_DIGIT_FIFTY, //
			ROMAN_NUMERAL_DIGIT_TEN, //
			ROMAN_NUMERAL_DIGIT_FIVE, //
			ROMAN_NUMERAL_DIGIT_ONE //
	};

	private static Integer[] valueOrderDescending = new Integer[ROMAN_NUMERAL_ORDER_DESCENDING.length];

	static {
		romanNumeralToDigitMap.put(ROMAN_NUMERAL_DIGIT_ONE_THOUSAND, 1000);
		romanNumeralToDigitMap.put(ROMAN_NUMERAL_DIGIT_FIVE_HUNDRED, 500);
		romanNumeralToDigitMap.put(ROMAN_NUMERAL_DIGIT_ONE_HUNDRED, 100);
		romanNumeralToDigitMap.put(ROMAN_NUMERAL_DIGIT_FIFTY, 50);
		romanNumeralToDigitMap.put(ROMAN_NUMERAL_DIGIT_TEN, 10);
		romanNumeralToDigitMap.put(ROMAN_NUMERAL_DIGIT_FIVE, 5);
		romanNumeralToDigitMap.put(ROMAN_NUMERAL_DIGIT_ONE, 1);

		digitToRomanNumeralMap = MapUtils.invertMap(romanNumeralToDigitMap);

		for (int i = 0; i < ROMAN_NUMERAL_ORDER_DESCENDING.length; i++) {
			valueOrderDescending[i] = romanNumeralToDigitMap
					.get(ROMAN_NUMERAL_ORDER_DESCENDING[i]);
		}

		oneLessMap.put("IV", 4);
		oneLessMap.put("IX", 9);
		oneLessMap.put("XL", 40);
		oneLessMap.put("XC", 90);
		oneLessMap.put("CD", 400);
		oneLessMap.put("CM", 900);
	}

	public static final Set<String> ROMAN_NUMERAL_VALUES = new HashSet<String>(
			Arrays.asList(ROMAN_NUMERAL_ORDER_DESCENDING));

	public static boolean looksLikeRomanNumeral(String candidate) {
		if (candidate == null) {
			return false;
		}

		char[] chars = candidate.toUpperCase().toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (!ROMAN_NUMERAL_VALUES.contains(String.valueOf(chars[i]))) {
				return false;
			}
		}
		return true;
	}

	public static Integer convertToNumber(String romanNumeral) {
		if (romanNumeral == null) {
			return null;
		}

		int runningTotal = 0;
		char[] chars = romanNumeral.toUpperCase().toCharArray();
		for (int i = 0; i < chars.length; i++) {
			boolean isEnd = (i + 1) >= chars.length;
			if (isEnd) {
				runningTotal += romanNumeralToDigitMap.get(String
						.valueOf(chars[i]));
			} else {
				// check for a one less option
				Integer oneLessValue = oneLessMap.get(String.valueOf(ArrayUtils
						.subarray(chars, i, i + 2)));
				if (oneLessValue != null) {
					runningTotal += oneLessValue;
					i++; // increment one extra
				} else {
					runningTotal += romanNumeralToDigitMap.get(String
							.valueOf(chars[i]));
				}
			}
		}

		return runningTotal;
	}

	public static String convertToRomanNumeral(Integer number) {
		return null;
	}
}
