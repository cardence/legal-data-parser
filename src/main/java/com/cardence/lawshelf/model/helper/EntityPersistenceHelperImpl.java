package com.cardence.lawshelf.model.helper;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cardence.lawshelf.model.Attribute;
import com.cardence.lawshelf.model.AttributeDao;
import com.cardence.lawshelf.model.Code;
import com.cardence.lawshelf.model.CodeAlias;
import com.cardence.lawshelf.model.CodeAliasDao;
import com.cardence.lawshelf.model.CodeCollection;
import com.cardence.lawshelf.model.CodeCollectionDao;
import com.cardence.lawshelf.model.CodeDao;
import com.cardence.lawshelf.model.ContentFull;
import com.cardence.lawshelf.model.ContentFullDao;
import com.cardence.lawshelf.model.ContentPart;
import com.cardence.lawshelf.model.ContentPartDao;
import com.cardence.lawshelf.model.Section;
import com.cardence.lawshelf.model.SectionDao;

@Component
public class EntityPersistenceHelperImpl implements EntityPersistenceHelper {

	@Autowired
	private Log log;

	@Autowired
	private CodeCollectionDao codeCollectionDao;

	@Autowired
	private CodeDao codeDao;

	@Autowired
	private SectionDao sectionDao;

	@Autowired
	private ContentFullDao contentFullDao;

	@Autowired
	private ContentPartDao contentPartDao;

	@Autowired
	private AttributeDao attributeDao;

	@Autowired
	private CodeAliasDao codeAliasDao;
	

	/* ****
	 * 
	 * STORE HANDLER METHODS
	 * 
	 * ***
	 */
	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.model.helper.EntityPersistenceHelper#storeCodeCollection(com.cardence.lawshelf.model.CodeCollection)
	 */
	public void storeCodeCollection(CodeCollection codeCollection) {
		assert codeCollection != null : "code collection is null";
		boolean isUpdate = true;

		if (codeCollection.getId() == null) {
			Collection<CodeCollection> foundCodeCollections = this.codeCollectionDao.findCodeCollectionsByNameAndYear(
					codeCollection.getName(), codeCollection.getYear());
			if (foundCodeCollections != null && !foundCodeCollections.isEmpty()) {
				// found one... update
				assert foundCodeCollections.size() == 1;
				CodeCollection foundCodeCol = (CodeCollection) foundCodeCollections.toArray()[0];
				codeCollection.setId(foundCodeCol.getId());
				this.codeCollectionDao.updateCodeCollection(codeCollection);
			} else {
				// did not find one... create
				Integer newId = this.codeCollectionDao.createCodeCollection(codeCollection);
				isUpdate = false;
				codeCollection.setId(newId);
			}
		} else {
			this.codeCollectionDao.updateCodeCollection(codeCollection);
		}

		// now store any attributes
		this.storeAttributes(codeCollection.getAttributes(), isUpdate);
	}

	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.model.helper.EntityPersistenceHelper#storeCode(com.cardence.lawshelf.model.Code)
	 */
	public void storeCode(Code code) {
		assert code != null : "code is null";

		boolean isUpdate = true;

		if (code.getId() == null) {
			Collection<Code> foundCodes = this.codeDao.findCodeByNameAndSequenceAndCollection(code.getName(),
					code.getCodeSequence(), code.getCodeCollectionId());

			if (foundCodes != null && !foundCodes.isEmpty()) {
				// found one... update
				assert foundCodes.size() == 1;
				Code foundCode = (Code) foundCodes.toArray()[0];
				code.setId(foundCode.getId());
				this.codeDao.updateCode(code);
			} else {
				// did not find one... create
				Integer newId = this.codeDao.createCode(code);
				isUpdate = false;
				code.setId(newId);
			}
		} else {
			this.codeDao.updateCode(code);
		}

		log.info("");
		if (isUpdate) {
			log.info("... updating code [" + code.getHeading() + "]");
		} else {
			log.info("NEW CODE RECORD: " + code.getHeading());
		}
		log.info("");

		// now take care of the aliases
		this.storeCodeAlias(code);

		// now store any attributes
		this.storeAttributes(code.getAttributes(), isUpdate);
	}

	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.model.helper.EntityPersistenceHelper#storeSection(com.cardence.lawshelf.model.Section)
	 */
	public void storeSection(Section section) {
		assert section != null : "section is null";

		if (section.getId() == null) {
			Section foundSection = this.sectionDao.findSectionByCodeAndReference(section.getCodeId(),
					section.getSourceReference());

			if (foundSection != null) {
				// found one... update
				section.setId(foundSection.getId());
				this.sectionDao.updateSection(section);
				section.setIsNewRecord(Boolean.FALSE);
			} else {
				// did not find one... create
				Integer newId = this.sectionDao.createSection(section);
				section.setIsNewRecord(Boolean.TRUE);
				section.setId(newId);
			}
		} else {
			this.sectionDao.updateSection(section);
			section.setIsNewRecord(Boolean.FALSE);
		}

		if (section.getIsNewRecord()) {
			log.info("SECTION CREATED: " + section.getSourceReference());
			log.info("   --> heading: " + section.getHeading());
		} else {
			log.info(" ... updating section [" + section.getHeading() + "]");
		}

		// now store any attributes
		this.storeAttributes(section.getAttributes(), !section.getIsNewRecord());
	}

	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.model.helper.EntityPersistenceHelper#storeContentFull(com.cardence.lawshelf.model.ContentFull, boolean)
	 */
	public void storeContentFull(ContentFull content, boolean isDeleteFirst) {
		assert content != null : "content full is null";
		boolean isUpdate = true;

		if (isDeleteFirst) {
			this.contentFullDao.deleteAllForSection(content.getSectionId());
		}

		if (content.getId() == null) {
			Integer newId = this.contentFullDao.createContentFull(content);
			content.setId(newId);
			isUpdate = false;
		} else {
			this.contentFullDao.updateContentFull(content);
		}

		// now store any attributes
		this.storeAttributes(content.getAttributes(), isUpdate);
	}

	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.model.helper.EntityPersistenceHelper#storeContentPart(com.cardence.lawshelf.model.ContentPart)
	 */
	public void storeContentPart(ContentPart content) {
		assert content != null : "content part is null";
		boolean isUpdate = true;

		if (content.getId() == null) {
			Integer newId = this.contentPartDao.createContentPart(content);
			isUpdate = false;
			content.setId(newId);
		} else {
			this.contentPartDao.updateContentPart(content);
		}

		// now store any attributes
		this.storeAttributes(content.getAttributes(), isUpdate);
	}

	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.model.helper.EntityPersistenceHelper#storeContentParts(int, int, java.util.Collection, boolean)
	 */
	public void storeContentParts(int sectionId, int contentFullId, Collection<ContentPart> contents,
			boolean isDeleteFirst) {
		if (isDeleteFirst) {
			this.contentPartDao.deleteAllAttributesForSection(sectionId);
			this.contentPartDao.deleteAllForSection(sectionId);
		}

		for (ContentPart part : contents) {
			part.setContentId(contentFullId);
			storeContentPart(part);
		}
	}

	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.model.helper.EntityPersistenceHelper#storeCodeAlias(com.cardence.lawshelf.model.Code)
	 */
	public void storeCodeAlias(Code code) {
		if (code == null || code.getAliases() == null) {
			return;
		}
		assert code.getId() != null : "code id cannot be null";

		for (Iterator<String> it = code.getAliases().iterator(); it.hasNext();) {
			CodeAlias ca = new CodeAlias();
			ca.setAliasname(it.next());
			ca.setCode(code);
			ca.setCodeId(code.getId());
			this.codeAliasDao.createAlias(ca);
		}
	}

	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.model.helper.EntityPersistenceHelper#storeAttributes(java.util.Collection, boolean)
	 */
	public void storeAttributes(Collection<Attribute> attrs, boolean isDeleteFirst) {
		if (attrs == null) {
			return;
		}
		if (isDeleteFirst) {
			this.attributeDao.replaceAllAttributes(attrs);
		} else {
			this.attributeDao.createAllAttributes(attrs);
		}
	}
	
}
