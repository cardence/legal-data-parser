package com.cardence.lawshelf.model.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_SINGLETON)
@Lazy(value = false)
public class SequenceTranslator implements ApplicationContextAware {
	private static final char[] LOWERCASE_LETTERS = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

	private static ApplicationContext appctx;
	private static SequenceTranslator me;

	public static SequenceTranslator getInstance() {
		if (me == null) {
			me = appctx.getBean(SequenceTranslator.class);
		}
		return me;
	}

	public String translateSequenceToLetter(int sequence) {
		int localSequence = sequence - 1;
		localSequence--;
		if (localSequence < 0) {
			localSequence = 0;
		} else if (localSequence >= LOWERCASE_LETTERS.length) {
			localSequence = LOWERCASE_LETTERS.length - 1;
		}
		return ("" + LOWERCASE_LETTERS[localSequence]).toUpperCase();

	}

	public int translateRomanNumeralToSequence(String romanNumeral) {
		if (RomanNumeralUtils.looksLikeRomanNumeral(romanNumeral)) {
			return RomanNumeralUtils.convertToNumber(romanNumeral);
		} else {
			return 0;
		}
	}

	public int translateLetterToSequence(String letters) {
		if (letters == null) {
			return 0;
		}
		final List<Integer> integers = new ArrayList<Integer>();
		char[] chars = letters.toLowerCase().toCharArray();
		for (char c : chars) {
			integers.add(translateLowerCaseLetterToInt(c));
		}
		Collections.reverse(integers);
		return calculateFinalBase26Value(integers.toArray(new Integer[0]));
	}

	public int translateLowerCaseLetterToInt(char letter) {
		return ArrayUtils.indexOf(LOWERCASE_LETTERS, letter) + 1;
	}

	public int calculateFinalBase26Value(Integer[] reverseOrderedIntegers) {
		int finalValue = 0;
		for (int index = 0; index < reverseOrderedIntegers.length; index++) {
			final int place = index;
			final int value = reverseOrderedIntegers[index];
			final Double d = Math.pow(26.0, place);
			finalValue += d.intValue() * value;

		}

		return finalValue;
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		appctx = applicationContext;
	}
}
