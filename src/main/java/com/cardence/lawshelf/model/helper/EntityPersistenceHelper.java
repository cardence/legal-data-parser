package com.cardence.lawshelf.model.helper;

import java.util.Collection;

import com.cardence.lawshelf.model.Attribute;
import com.cardence.lawshelf.model.Code;
import com.cardence.lawshelf.model.CodeCollection;
import com.cardence.lawshelf.model.ContentFull;
import com.cardence.lawshelf.model.ContentPart;
import com.cardence.lawshelf.model.Section;

public interface EntityPersistenceHelper {

	/* ****
	 * 
	 * STORE HANDLER METHODS
	 * 
	 * ***
	 */
	public abstract void storeCodeCollection(CodeCollection codeCollection);

	public abstract void storeCode(Code code);

	public abstract void storeSection(Section section);

	public abstract void storeContentFull(ContentFull content, boolean isDeleteFirst);

	public abstract void storeContentPart(ContentPart content);

	public abstract void storeContentParts(int sectionId, int contentFullId, Collection<ContentPart> contents,
			boolean isDeleteFirst);

	public abstract void storeCodeAlias(Code code);

	public abstract void storeAttributes(Collection<Attribute> attrs, boolean isDeleteFirst);

}