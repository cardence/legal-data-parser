package com.cardence.lawshelf.model;

public class Section extends BaseDatabaseObject {

	private String heading;
	private String shortHeading;
	private String textHeading;
	private String levelType;
	private String levelPosition;
	private String sourceReference;
	private int sectionSequence;

	private Code code;
	private Section parent;
	private Integer parentSectionId;
	private Integer codeId;

	private Boolean isNewRecord;
	private String parentLevelPosition;

	public String getSourceReference() {
		return sourceReference;
	}

	public void setSourceReference(String sourceReference) {
		this.sourceReference = sourceReference;
	}

	public String getHeading() {
		if (heading != null) {
			return heading;
		} else if (textHeading != null) {
			return textHeading;
		} else if (shortHeading != null) {
			return shortHeading;
		}
		return null;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public void setShortHeading(String shortheading) {
		this.shortHeading = shortheading;
	}

	public String getShortHeading() {
		return (this.shortHeading != null) ? this.shortHeading : getHeading();
	}

	public String getLevelType() {
		return levelType;
	}

	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	public String getLevelPosition() {
		return levelPosition;
	}

	public void setLevelPosition(String levelPosition) {
		this.levelPosition = levelPosition;
	}

	public int getSectionSequence() {
		return sectionSequence;
	}

	public void setSectionSequence(int sectionSequence) {
		this.sectionSequence = sectionSequence;
	}

	public Integer getCodeId() {
		return (getCode() == null) ? codeId : getCode().getId();
	}

	public void setCodeId(Integer id) {
		this.codeId = id;
	}

	public Integer getParentSectionId() {
		return (getParent() == null) ? parentSectionId : getParent().getId();
	}

	public void setParentSectionId(Integer parentSectionId) {
		this.parentSectionId = parentSectionId;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public Section getParent() {
		return parent;
	}

	public void setParent(Section parent) {
		this.parent = parent;
	}

	public Boolean getIsNewRecord() {
		return isNewRecord;
	}

	public void setIsNewRecord(Boolean isNewRecord) {
		this.isNewRecord = isNewRecord;
	}

	@Override
	protected String getAttributeType() {
		return "SECTION";
	}

	@Override
	public String toString() {
		return "Section [heading=" + getHeading() + ", shortheading=" + getShortHeading() + ", textheading="
				+ getTextHeading() + ", levelType=" + getLevelType() + ", levelPosition=" + getLevelPosition()
				+ ", sourceReference=" + getSourceReference() + ", sectionSequence=" + getSectionSequence()
				+ ", codeId=" + getCodeId() + ", parentSectionId=" + getParentSectionId() + ", toString()="
				+ super.toString() + "]";
	}

	public String getParentLevelPosition() {
		return parentLevelPosition;
	}

	public void setParentLevelPosition(String parentLevelPosition) {
		this.parentLevelPosition = parentLevelPosition;
	}

	public String getTextHeading() {
		if (textHeading != null) {
			return textHeading;
		} else if (heading != null) {
			return heading;
		} else if (shortHeading != null) {
			return shortHeading;
		}
		return null;
	}

	public void setTextHeading(String textHeading) {
		this.textHeading = textHeading;
	}

}
