package com.cardence.lawshelf.model;

public class UscCollection extends CodeCollection {

	public static final String USC_TITLE = "United States Code";
	public static final String USC_ABBR = "U.S. Code ";

	public UscCollection() {
		super();

		super.setName(USC_TITLE);
		super.setStateCode(null);
		super.setFederal(true);
	}

	@Override
	public String getHeading() {
		return USC_ABBR + super.getYear();
	}

	@Override
	protected String getSourceOfData() {
		return "USC";
	}
}
