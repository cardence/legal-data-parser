package com.cardence.lawshelf.model;

import java.util.Collection;

public interface CodeDao {

	Code findCode(int id);

	Collection<Code> findCodeByCollection(Integer id);

	Collection<Code> findCodeByNameAndSequenceAndCollection(String name,
			Integer seq, Integer id);

	void updateCode(Code code);

	int createCode(Code code);

	void deleteCode(int id);
}
