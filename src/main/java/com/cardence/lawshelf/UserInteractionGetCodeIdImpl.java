package com.cardence.lawshelf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cardence.lawshelf.model.Code;
import com.cardence.lawshelf.model.CodeCollection;
import com.cardence.lawshelf.model.CodeCollectionDao;
import com.cardence.lawshelf.model.CodeDao;

@Component
public class UserInteractionGetCodeIdImpl extends UserInteractionImpl implements UserInteractionGetCodeId {

	private final String PROCESS_ALL_INT = "000";

	@Autowired
	private Log log;

	@Autowired
	private CodeCollectionDao codeCollectionDao;

	@Autowired
	private CodeDao codeDao;

	public List<Integer> getAllCodeIds(int collectionId) {
		List<Integer> codeIdList = new ArrayList<Integer>();

		// go and find the codes for the selected collection
		final Collection<Code> codes = codeDao.findCodeByCollection(collectionId);
		for (Code code : codes) {
			codeIdList.add(code.getId());
		}

		return codeIdList;
	}

	public List<Integer> getAllCodeIds() {
		List<Integer> codeIdList = new ArrayList<Integer>();

		final Collection<CodeCollection> codeCollection = codeCollectionDao.findAllCodeCollections();

		for (CodeCollection cc : codeCollection) {
			// go and find the codes for the selected collection
			final Collection<Code> codes = codeDao.findCodeByCollection(cc.getId());
			for (Code code : codes) {
				codeIdList.add(code.getId());
			}
		}

		return codeIdList;
	}

	public int getDesiredCodeId(int collectionId) {

		// go and find the codes for the selected collection
		final Collection<Code> codes = codeDao.findCodeByCollection(collectionId);

		// create the choices
		UserChoicePresenter selectCode = new UserChoicePresenter() {
			public void printChoices() {
				for (Code code : codes) {
					System.out.println("[" + code.getId() + "] " + code.getHeading());
				}
			}
		};

		// ask the question
		String codeIdStr = super.askQuestion("Select the code to work with", selectCode);
		System.out.println("you chose [" + codeIdStr + "]");

		// massage response
		Integer codeId = null;
		if (NumberUtils.isNumber(codeIdStr)) {
			codeId = NumberUtils.createInteger(codeIdStr);
		} else {
			System.out.println("Not a valid Code ID: " + codeIdStr);
			System.exit(1);
		}
		return codeId;
	}

}
