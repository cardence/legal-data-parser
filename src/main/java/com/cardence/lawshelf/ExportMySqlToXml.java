package com.cardence.lawshelf;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.cardence.lawshelf.ios.ConvertCodeToXml;
import com.cardence.lawshelf.ios.xml.Asset;
import com.cardence.lawshelf.ios.xml.ObjectFactory;

@Component
public class ExportMySqlToXml {

	private static ExportMySqlToXml me;

	/* BEGIN: Spring Configured Variables */

	private String exportPath;

	/* END: Spring Configured Variables */

	@Autowired
	private UserInteractionGetCodeId uiGetCodeId;

	@Autowired
	private UserInteractionGetContinue uiShouldContinue;

	@Autowired
	private UserInteractionShouldProcessAll uiShouldProcessAll;

	@Autowired
	private UserInteractionGetCollectionId uiGetCollectionId;

	@Autowired
	private ConvertCodeToXml converter;

	@Autowired
	private Log log;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/app-context.xml");

		me = context.getBean("ExportMySqlToXml", ExportMySqlToXml.class);

		Assert.notNull(me, "ExportMySqlToXml not created");

		me.run();
	}

	public void run() {
		Assert.notNull(log, "Log not created");
		try {

			// UserInteractionGetCodeId user = new
			// UserInteractionGetCodeIdImpl();
			if (uiShouldProcessAll.getShouldProcessAll()) {
				// process ALL codes for all collections
				// this is probably used more when there isn't too much overlap
				// (i.e. multiple us codes)
				// in the db
				List<Integer> codeIds = uiGetCodeId.getAllCodeIds();
				for (Integer codeId : codeIds) {
					Asset asset = converter.convertCodeWithID(codeId);
					writeAsset(asset, true);
					writeAsset(asset, false);
				}
			} else {
				do {
					// select a collection -> this is the context for the next
					// questions
					int collectionId = uiGetCollectionId.getDesiredCollectionId();
					if (uiShouldProcessAll.getShouldProcessAll()) {
						// process all codes for just this collection
						List<Integer> codeIds = uiGetCodeId.getAllCodeIds(collectionId);
						for (Integer codeId : codeIds) {
							Asset asset = converter.convertCodeWithID(codeId);
							writeAsset(asset, true);
							writeAsset(asset, false);
						}
					} else {
						// process on an individual level
						do {
							int codeId = uiGetCodeId.getDesiredCodeId(collectionId);

							Asset asset = converter.convertCodeWithID(codeId);
							writeAsset(asset, true);
							writeAsset(asset, false);

						} while (uiShouldContinue.getShouldContinue());
					}
				} while (uiShouldContinue.getShouldContinue());
			}

			System.out.println("############################################");
			System.out.println("############################################");
			System.out.println("");
			System.out.println("");
			System.out.println(" -                 COMPLETE               - ");
			System.out.println("");
			System.out.println("");
			System.out.println("############################################");
			System.out.println("############################################");
			// log.info("PROCESS CODE ID: " + codeId);
		} catch (Exception e) {
			log.error("Caught an error", e);
		}
	}

	public void writeAsset(Asset asset, boolean format) {
		FileOutputStream fout = null;
		BufferedOutputStream bout = null;
		File file = null;

		try {

			String identifier = StringUtils.replaceChars(asset.getAbbr(), " ", "");
			identifier = StringUtils.replaceChars(identifier, ".", "");

			String fileEnd = "-" + identifier + ".xml";

			String filename = "";
			if (format) {
				filename = getExportPath() + "asset-formatted" + fileEnd;
			} else {
				filename = getExportPath() + "asset" + fileEnd;

			}
			file = new File(filename);
			log.info("Writing to file: " + filename);

			ObjectFactory factory = new ObjectFactory();
			JAXBElement<Asset> jaxbRoot = factory.createAsset(asset);

			JAXBContext ctx = JAXBContext.newInstance(asset.getClass());
			Marshaller marshaller = ctx.createMarshaller();

			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, format);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "LawShelfIDX.xsd");

			fout = new FileOutputStream(file);
			bout = new BufferedOutputStream(fout);
			marshaller.marshal(jaxbRoot, bout);
			bout.flush();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bout != null) {
				try {
					bout.close();
					bout = null;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (fout != null) {
				try {
					fout.close();
					fout = null;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (file != null) {
				fout = null;
			}
		}
	}

	public String getExportPath() {
		return exportPath;
	}

	public void setExportPath(String exportPath) {
		this.exportPath = exportPath;
	}

}
