package com.cardence.lawshelf;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

import com.cardence.lawshelf.html.HtmlParser;
import com.cardence.lawshelf.pdf.PdfParser;

public class SupremeCourtLoaderStartup {

	private static SupremeCourtLoaderStartup me;

	/* BEGIN: Spring Configured Variables */
	
	private String targetPath; 

	private String targetTest; 

	/* END: Spring Configured Variables */

	@Autowired
	private PdfParser parser;

	@Autowired
	private Log log;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"META-INF/spring/app-context.xml");

		me = context.getBean("SupremeCourtLoader", SupremeCourtLoaderStartup.class);
		Assert.notNull(me, "SupremeCourtLoader not created");

		me.run();
	}

	private void executeForTest() throws Exception {
		log.debug("Using target test path: " + targetTest);

		URL fileURL = Thread.currentThread().getContextClassLoader()
				.getResource(targetTest);
		Assert.notNull(fileURL, "File URL Cannot be null");

		File file = new File(fileURL.toURI());
		if (!file.exists()) {
			log.error("ERROR: No file found at " + targetTest);
			return;
		}
		if (!file.canRead()) {
			log.error("ERROR: NOT READABLE at " + targetTest);
			return;
		}

		if (file.isFile()) {
			log.info("Target is a file... Processing file: " + file.getName());
			processFile(file);
		} else {
			log.error("Could not determine if the target is a file or a directory... Exiting");
		}

	}

	private void executeForProduction() throws Exception {
		log.debug("Using target path: " + targetPath);

		File file = new File(targetPath);
		if (!file.exists()) {
			log.error("ERROR: No file found at " + targetPath);
			return;
		}
		if (!file.canRead()) {
			log.error("ERROR: NOT READABLE at " + targetPath);
			return;
		}
		if (file.isDirectory()) {
			log.info("Target is directory... Proceeding to iterate through each file");
			File[] filearray = file.listFiles();
			for (File f : filearray) {
				String name = f.getName();
				if (name.toLowerCase().endsWith(".pdf")) {
					log.info("Processing file: " + f.getName());
					processFile(f);
				}
			}
		} else if (file.isFile()) {
			log.info("Target is a file... Processing file: " + file.getName());
			processFile(file);
		} else {
			log.error("Could not determine if the target is a file or a directory... Exiting");
		}
	}

	public void run() {
		Assert.notNull(parser, "Parser not created");
		Assert.notNull(log, "Log not created");
		Assert.state(targetPath != null || targetTest != null,
				"Target path or test is Null");

		try {
			if (targetTest != null) {
				executeForTest();
			} else if (targetPath != null) {
				executeForProduction();
			}

		} catch (Exception e) {
			log.error("Caught an error", e);
		}
	}

	private void processFile(File file) throws IOException {

		parser.parseFile(file);

		parser.processStructure();
	}

	public String getTargetPath() {
		return targetPath;
	}

	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}

	public String getTargetTest() {
		return targetTest;
	}

	public void setTargetTest(String targetTest) {
		this.targetTest = targetTest;
	}

}
