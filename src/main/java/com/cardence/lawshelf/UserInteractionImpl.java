package com.cardence.lawshelf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInteractionImpl implements UserInteraction {

	public UserInteractionImpl() {
		super();
	}

	public String askQuestion(String question, UserChoicePresenter choices) {
		try {
			System.out.println("");
			System.out.println("***************************");
			System.out.println("         QUESTION          ");
			System.out.println("***************************");
			System.out.println("");
			System.out.println(question);
			System.out.println("");
			choices.printChoices();
			System.out.println("");
			System.out.println("[XXX] Exit");
			System.out.println("");
			System.out.print("--> ");

			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String answer = null;
			try {
				answer = br.readLine();
			} catch (IOException e) {
				System.out.println("Error! [" + e.getLocalizedMessage() + "]");
				System.exit(1);
			}

			if (answer == null) {
				System.out.println("Could not decipher null response");
				System.exit(1);
			}

			answer = answer.trim();

			if ("XXX".equals(answer.toUpperCase())) {
				System.exit(0);
			}

			return answer;
		} catch (Throwable e) {
			throw new RuntimeException(e.getLocalizedMessage());
		}
	}
}