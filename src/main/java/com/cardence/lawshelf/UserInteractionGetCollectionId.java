package com.cardence.lawshelf;

public interface UserInteractionGetCollectionId {

	public abstract int getDesiredCollectionId();

}