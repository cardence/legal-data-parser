package com.cardence.lawshelf;

public interface UserInteractionGetContinue extends UserInteraction {

	boolean getShouldContinue();
}