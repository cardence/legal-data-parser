package com.cardence.lawshelf.html;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class MetaNode {

	private String name;
	private String type;
	private ArrayList<MetaNode> children;

	public MetaNode(String name, String type) {
		super();
		this.name = name;
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetaNode other = (MetaNode) obj;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!children.equals(other.children))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<MetaNode> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<MetaNode> children) {
		this.children = children;
	}
	
	public void setChildren(Collection<MetaNode> children) {
		this.children = new ArrayList<MetaNode>(children);
	}

	public void addChild(MetaNode node) {
		if (children == null) {
			children = new ArrayList<MetaNode>();
		}
		children.add(node);
	}

	@Override
	public String toString() {
		return toString("", false);
	}

	public String toString(String indent, boolean newline) {
		String s = indent;
		if (newline) {
			s += "\n";
		}
		if (children == null) {
			s += "<" + this.getType() + " name='" + this.getName() + "' />";
		} else {
			s += "<" + this.getType() + " name='" + this.getName() + "'>";
			for (MetaNode n : children) {
				s += n.toString(indent + indent, newline);
			}
			if (newline) {
				s += "\n";
			}
			s += "</" + this.getType() + ">";
		}
		return s;
	}

}