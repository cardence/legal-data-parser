package com.cardence.lawshelf.html;

import java.io.File;
import java.net.URL;
import java.util.Map;

public interface HtmlParserHandler {

	void beginDocument();

	void endDocument();

	void beginHead();

	void endHead();

	void beginBody();

	void endBody();

	void foundComment(String comment);

	void foundElement(String tagname, String innerHTML, String outerHTML,
			String text, Map<String, String> attributeMap);

	void beginElementChildren();

	void endElementChildren();

	/**
	 * Opportunity to preprocess the actual source file. Return value cannot be
	 * a File, because we do not want to actually modify the source, only
	 * provide a "filter" of the original source.
	 * 
	 * @param file
	 * @return HTML String: null, if no changes; otherwise, return the modified
	 *         string.
	 */
	String willProcessFile(File file);

	/**
	 * Opportunity to preprocess the actual source file. Return value cannot be
	 * a URL, because we do not want to actually modify the source, only provide
	 * a "filter" of the original source.
	 * 
	 * @param url
	 * @return HTML String: null, if no changes; otherwise, return the modified
	 *         string.
	 */
	String willProcessUrl(URL url);

	/**
	 * Opportunity to preprocess the actual source file
	 * 
	 * @param html
	 * @return
	 */
	String willProcessHtml(String html);
}
