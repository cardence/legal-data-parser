package com.cardence.lawshelf.html;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public interface HtmlParser {

	void setParserHandler(HtmlParserHandler handler);

	void parseFile(String filepath) throws IOException;

	void parseFile(File file) throws IOException;

	void parseUrl(URL url) throws IOException;

	void parseHtmlString(String html);

	void processStructure();

}
