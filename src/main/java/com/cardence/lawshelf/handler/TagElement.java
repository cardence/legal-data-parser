package com.cardence.lawshelf.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;

public class TagElement {

	private static final Set<String> HEADING_TAGS = new HashSet<String>(
			Arrays.asList("h1", "h2", "h3", "h4", "h5", "h6"));

	private static final Set<String> TABLE_TAGS = new HashSet<String>(
			Arrays.asList("table", "tr", "td", "tbody", "tfoot"));

	private static final String HTML_P = "p";
	private static final String HTML_DIV = "div";
	private static final String HTML_TABLE = "table";
	private static final String HTML_ATTR_ID = "id";
	private static final String HTML_ATTR_CLASS = "class";

	private String tagname;
	private String tagvalue;
	private Map<String, String> attributeMap;
	private String outerHTML;
	private String innerHTML;
	private List<TagElement> subTagElements;

	public String getOuterHTML() {
		return getOuterHTML(false);
	}

	public String getOuterHTML(boolean escape) {
		return (escape) ? StringEscapeUtils.escapeHtml(outerHTML) : outerHTML;
	}

	public void setOuterHTML(String outerHTML) {
		this.outerHTML = outerHTML;
	}

	public String getInnerHTML() {
		return getInnerHTML(false);
	}

	public String getInnerHTML(boolean escape) {
		return (escape) ? StringEscapeUtils.escapeHtml(innerHTML) : innerHTML;
	}

	public void setInnerHTML(String innerHTML) {
		this.innerHTML = innerHTML;
	}

	public String getTagname() {
		return tagname;
	}

	public void setTagname(String tagname) {
		this.tagname = tagname;
	}

	public String getTagvalue() {
		return tagvalue;
	}

	public void setTagvalue(String tagvalue) {
		this.tagvalue = tagvalue;
	}

	public Map<String, String> getAttributeMap() {
		if (attributeMap == null) {
			attributeMap = new HashMap<String, String>();
		}
		return attributeMap;
	}

	public void setAttributeMap(Map<String, String> attributeMap) {
		this.attributeMap = attributeMap;
	}

	public void addTagElement(TagElement element) {
		getSubTagElements().add(element);
	}

	public List<TagElement> getSubTagElements() {
		if (subTagElements == null) {
			subTagElements = new ArrayList<TagElement>();
		}
		return subTagElements;
	}

	public void setSubTagElements(List<TagElement> subTagElements) {
		this.subTagElements = subTagElements;
	}

	public String getHtmlClass() {
		return getAttributeMap().get(HTML_ATTR_CLASS);
	}

	public String getHtmlId() {
		return getAttributeMap().get(HTML_ATTR_ID);
	}

	public boolean isHeading() {
		return (tagname != null && HEADING_TAGS.contains(tagname.toLowerCase()));
	}

	public boolean isDiv() {
		return (tagname != null && HTML_DIV.equals(tagname.toLowerCase()));
	}

	public boolean isParagraph() {
		return (tagname != null && HTML_P.equals(tagname.toLowerCase()));
	}

	public boolean isTable() {
		return (tagname != null && HTML_TABLE.equals(tagname.toLowerCase()));
	}

	public boolean isTableRelated() {
		return (tagname != null && TABLE_TAGS.contains(tagname.toLowerCase()));
	}

}
