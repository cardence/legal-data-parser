package com.cardence.lawshelf.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.cardence.lawshelf.model.Code;
import com.cardence.lawshelf.model.CodeCollection;
import com.cardence.lawshelf.model.ContentFull;
import com.cardence.lawshelf.model.ContentPart;
import com.cardence.lawshelf.model.Section;

public class ModelTracker {

	// these 2 stay constant throughout
	private CodeCollection codeCollection;
	private Code code;

	// the idea here is to save a section at a time

	// these 2 items stay constant just while the content items are collected
	private Section section;

	// this one gets collected throughout, but contains original HTML
	private ContentFull originalContent;

	// this one gets collected throughout
	private ArrayList<ContentPart> contentParts;

	// some kind of provider specific key, with then the actual PK from the db
	private HashMap<String, Integer> completePrimaryKeyMapping;

	// some kind of provider specific key, with then the actual PK from the db
	private HashMap<String, String> completeLevelPositionMapping;

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public ContentFull getContentFull() {
		return originalContent;
	}

	public void setContentFull(ContentFull originalContent) {
		this.originalContent = originalContent;
	}

	public void addContentFull(String moreContent) {
		this.originalContent.addContent(moreContent);
	}

	public ContentPart getWorkingContent() {
		ArrayList<ContentPart> parts = this.getContentParts();
		if (parts.size() == 0) {
			return null;
		}
		return parts.get(parts.size() - 1);
	}

	public ArrayList<ContentPart> getContentParts() {
		if (contentParts == null) {
			contentParts = new ArrayList<ContentPart>();
		}
		return contentParts;
	}

	public void setContentParts(ArrayList<ContentPart> contentParts) {
		this.contentParts = contentParts;
	}

	public void addContentPart(ContentPart content) {
		this.getContentParts().add(content);
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public CodeCollection getCodeCollection() {
		return codeCollection;
	}

	public void setCodeCollection(CodeCollection codeCollection) {
		this.codeCollection = codeCollection;
	}

	public Integer findPrimaryKey(String key) {
		return getCompletePrimaryKeyMapping().get(key);
	}

	public void addPrimaryKeyMapping(String key, Integer dbPK) {
		getCompletePrimaryKeyMapping().put(key, dbPK);
	}

	public Map<String, Integer> getCompletePrimaryKeyMapping() {
		if (this.completePrimaryKeyMapping == null) {
			this.completePrimaryKeyMapping = new HashMap<String, Integer>();
		}
		return this.completePrimaryKeyMapping;
	}

	public String findLevelPosition(String key) {
		return getCompleteLevelPositionMapping().get(key);
	}

	public void addLevelPositionMapping(String key, String lp) {
		getCompleteLevelPositionMapping().put(key, lp);
	}

	public Map<String, String> getCompleteLevelPositionMapping() {
		if (this.completeLevelPositionMapping == null) {
			this.completeLevelPositionMapping = new HashMap<String, String>();
		}
		return this.completeLevelPositionMapping;
	}

}
