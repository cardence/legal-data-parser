package com.cardence.lawshelf.handler;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public interface UscTags {

	String FORMATTYPE_HTML = "text/html";
	String FORMATTYPE_PLAIN = "text/plain";

	String FIELD_START_TAG = "field-start";
	String FIELD_END_TAG = "field-end";
	String EXPCITE_TAG = "expcite";
	String DOCID_TAG = "documentid";
	String USCKEY_TAG = "usckey";
	String ITEMPATH_TAG = "itempath";
	String ITEMSORT_TAG = "itemsortkey";

	String HEAD_FIELD_TAG = "head";
	String TITLE_HEAD_FIELD_TAG = "titlehead";
	String STRUCTURAL_HEAD_FIELD_TAG = "structuralhead";
	String REPEAL_HEAD_FIELD_TAG = "repealedhead";
	String REORG_HEAD_FIELD_TAG = "reorganizationplan-head";
	String LARGEITEMHEAD_FIELD_TAG = "largeitemhead";

	String NOTES_FIELD_TAG = "notes";
	String STATUTE_FIELD_TAG = "statute";
	String SOURCECREDIT_FIELD_TAG = "sourcecredit";
	String FOOTNOTE_FIELD_TAG = "footnote";
	String ANALYSIS_FIELD_TAG = "analysis";
	String TOC_FIELD_TAG = "toc";
	String TITLE_ENACT_CREDIT = "titleenactmentcredit";
	String REPEAL_SUMMARY = "repealsummary";
	String REORG_FIELD_TAG = "reorganizationplan";
	String APPDX_EFFDATE = "appendix-effectivedate";

	String LEVEL_TYPE_TITLE = "TITLE";
	String LEVEL_TYPE_SECTION = "SECTION";
	String LEVEL_TYPE_CHAPTER = "CHAPTER";
	String LEVEL_TYPE_SUBCHAPTER = "SUBCHAPTER";
	String LEVEL_TYPE_PART = "PART";
	String LEVEL_TYPE_SUBPART = "SUBPART";
	String LEVEL_TYPE_RULE = "RULE";

	Set<String> LEVEL_TYPE_SECTION_MATCH = new HashSet<String>(Arrays.asList(
			"SEC.", "SECS."));

	Set<String> LEVEL_TYPE_WITH_ROMAN_NUMERALS = new HashSet<String>(
			Arrays.asList(LEVEL_TYPE_SUBCHAPTER, LEVEL_TYPE_PART, LEVEL_TYPE_SUBPART));

}
