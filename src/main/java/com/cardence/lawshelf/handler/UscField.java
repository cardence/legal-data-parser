package com.cardence.lawshelf.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UscField implements UscTags {

	private static final Set<String> IDENTIFIER = new HashSet<String>(
			Arrays.asList(HEAD_FIELD_TAG, REPEAL_HEAD_FIELD_TAG,
					TITLE_HEAD_FIELD_TAG, STRUCTURAL_HEAD_FIELD_TAG));

	private static final Set<String> SECTION_IDENTIFIER = new HashSet<String>(
			Arrays.asList(HEAD_FIELD_TAG, REPEAL_HEAD_FIELD_TAG));

	private static final Set<String> SECTION_LIVE_IDENTIFIER = new HashSet<String>(
			Arrays.asList(HEAD_FIELD_TAG));

	private static final Set<String> SECTION_REPEALED_IDENTIFIER = new HashSet<String>(
			Arrays.asList(REPEAL_HEAD_FIELD_TAG));

	private static final Set<String> CODE_IDENTIFIER = new HashSet<String>(
			Arrays.asList(TITLE_HEAD_FIELD_TAG));

	private static final Set<String> STRUCTURE_IDENTIFIER = new HashSet<String>(
			Arrays.asList(STRUCTURAL_HEAD_FIELD_TAG));

	private static final Set<String> SECTION_TAGS = new HashSet<String>(
			Arrays.asList(STATUTE_FIELD_TAG, HEAD_FIELD_TAG,
					ANALYSIS_FIELD_TAG, FOOTNOTE_FIELD_TAG, NOTES_FIELD_TAG,
					SOURCECREDIT_FIELD_TAG, REPEAL_SUMMARY,
					REPEAL_HEAD_FIELD_TAG));

	private static final Set<String> STRUCTURE_TAGS = new HashSet<String>(
			Arrays.asList(STRUCTURAL_HEAD_FIELD_TAG, ANALYSIS_FIELD_TAG,
					NOTES_FIELD_TAG));

	private static final Set<String> CODE_TAGS = new HashSet<String>(
			Arrays.asList(TITLE_HEAD_FIELD_TAG, ANALYSIS_FIELD_TAG,
					NOTES_FIELD_TAG, TITLE_ENACT_CREDIT));

	private static final Set<String> CONSIDER_AS_HEADER = new HashSet<String>(
			Arrays.asList(HEAD_FIELD_TAG, TITLE_HEAD_FIELD_TAG,
					REORG_HEAD_FIELD_TAG, STRUCTURAL_HEAD_FIELD_TAG,
					REPEAL_HEAD_FIELD_TAG, LARGEITEMHEAD_FIELD_TAG));

	private static final Set<String> CONSIDER_AS_CONTENT = new HashSet<String>(
			Arrays.asList(REPEAL_SUMMARY, STATUTE_FIELD_TAG, REORG_FIELD_TAG));

	private static final Set<String> CONSIDER_AS_NOTES = new HashSet<String>(
			Arrays.asList(ANALYSIS_FIELD_TAG, TOC_FIELD_TAG,
					FOOTNOTE_FIELD_TAG, SOURCECREDIT_FIELD_TAG,
					NOTES_FIELD_TAG, TITLE_ENACT_CREDIT, APPDX_EFFDATE));

	private String fieldName;
	private List<TagElement> tagElementList;
	private List<UscField> children;
	private UscField parent;

	public List<TagElement> getTagElementList() {
		if (tagElementList == null) {
			tagElementList = new ArrayList<TagElement>();
		}
		return tagElementList;
	}

	public void addTagElement(TagElement element) {
		getTagElementList().add(element);
	}

	public String getTopLevelFieldName() {
		if (this.parent != null) {
			return this.parent.getTopLevelFieldName();
		} else {
			return this.fieldName;
		}
	}

	public UscField getParent() {
		return this.parent;
	}

	public List<UscField> getChildren() {
		if (children == null) {
			children = new ArrayList<UscField>();
		}
		return children;
	}

	public void addChild(UscField child) {
		getChildren().add(child);
		child.parent = this;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public boolean isStructural() {
		return (fieldName != null && STRUCTURE_IDENTIFIER.contains(fieldName));
	}

	public boolean isRepealedSection() {
		return (fieldName != null && SECTION_REPEALED_IDENTIFIER
				.contains(fieldName));
	}

	public boolean isSection() {
		return (fieldName != null && SECTION_IDENTIFIER.contains(fieldName));
	}

	public boolean isRegularSection() {
		return (fieldName != null && SECTION_LIVE_IDENTIFIER
				.contains(fieldName));
	}

	public boolean isHeading() {
		return (fieldName != null && CONSIDER_AS_HEADER.contains(fieldName));
	}

	public boolean isCode() {
		return (fieldName != null && CODE_IDENTIFIER.contains(fieldName));
	}

	public boolean isNotesMain() {
		return (fieldName != null && fieldName.equals(NOTES_FIELD_TAG));
	}

	public boolean isNotes() {
		return (fieldName != null && CONSIDER_AS_NOTES.contains(fieldName));
	}

	public boolean isContent() {
		return (fieldName != null && CONSIDER_AS_CONTENT.contains(fieldName));
	}

	public boolean isPartOfNotes() {
		return (isNotes() || (this.parent != null && this.parent
				.isPartOfNotes()));
	}

	public boolean isPartOfSection() {
		return (isSection() || (this.parent != null && this.parent
				.isPartOfSection()));
	}

	public boolean isPartOfCode() {
		return (isCode() || (this.parent != null && this.parent.isPartOfCode()));
	}

	public boolean isPartOfRepealedSection() {
		return (isRepealedSection() || (this.parent != null && this.parent
				.isPartOfRepealedSection()));
	}

	public boolean isPartOfRegularSection() {
		return (isRegularSection() || (this.parent != null && this.parent
				.isPartOfRegularSection()));
	}

	public boolean isPartOfStructural() {
		return (isStructural() || (this.parent != null && this.parent
				.isPartOfStructural()));
	}

	public boolean isKnownFieldKey() {
		String key = fieldName;
		return (IDENTIFIER.contains(key) || SECTION_IDENTIFIER.contains(key)
				|| SECTION_LIVE_IDENTIFIER.contains(key)
				|| SECTION_REPEALED_IDENTIFIER.contains(key)
				|| CODE_IDENTIFIER.contains(key)
				|| STRUCTURE_IDENTIFIER.contains(key)
				|| STRUCTURE_TAGS.contains(key) || SECTION_TAGS.contains(key)
				|| CONSIDER_AS_CONTENT.contains(key)
				|| CONSIDER_AS_HEADER.contains(key) || CONSIDER_AS_NOTES
					.contains(key));
	}

}
