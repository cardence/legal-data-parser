package com.cardence.lawshelf;

public interface UserInteractionShouldProcessAll extends UserInteraction {

	boolean getShouldProcessAll();
}