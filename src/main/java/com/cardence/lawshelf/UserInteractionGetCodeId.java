package com.cardence.lawshelf;

import java.util.List;

public interface UserInteractionGetCodeId extends UserInteraction {

	int getDesiredCodeId(int collectionId);

	List<Integer> getAllCodeIds(int collectionId);

	List<Integer> getAllCodeIds();
}