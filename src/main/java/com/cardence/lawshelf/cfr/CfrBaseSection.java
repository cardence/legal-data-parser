package com.cardence.lawshelf.cfr;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.cardence.lawshelf.cfr.jaxb.HD;
import com.cardence.lawshelf.model.helper.RomanNumeralUtils;
import com.cardence.lawshelf.model.helper.SequenceTranslator;

public class CfrBaseSection implements CfrSection {

	protected CfrSectionTracker sectionTracker = CfrSectionTracker.getInstance();

	protected SequenceTranslator sequenceTranslator = SequenceTranslator.getInstance();

	private String parentReference;
	private String heading;

	protected CfrBaseSection(String heading, String parentReference) {
		this.heading = heading;
		this.parentReference = parentReference;
	}

	public static final CfrSection getCfrSectionForHeading(String heading) {
		return new CfrBaseSection(heading, null);
	}

	protected static HD findHD(List<Object> unknownObjects) {
		for (Object unknown : unknownObjects) {
			if (unknown instanceof HD) {
				return ((HD) unknown);
			}
		}
		return null;
	}

	protected static String findAlternativeToHD(List<Object> unknownObjects) {
		for (Object unknown : unknownObjects) {
			if (unknown instanceof JAXBElement) {
				final JAXBElement jaxb = (JAXBElement) unknown;
				if (StringUtils.equals("RESERVED", jaxb.getName().getLocalPart())) {
					return jaxb.getValue().toString();
				}
			}
		}

		return findAnyString(unknownObjects);
	}

	protected static String findAnyString(List<Object> unknownObjects) {
		for (Object unknown : unknownObjects) {
			if (unknown instanceof String) {
				return (String) unknown;
			}
		}

		return null;

	}

	protected String getParentReference() {
		return this.parentReference;
	}

	public String getHeading() {
		return heading;
	}

	public int getSequence() {
		final String categoryLevel = getCategoryLevel();
		if (NumberUtils.isDigits(categoryLevel)) {
			return NumberUtils.toInt(categoryLevel);
		} else if (RomanNumeralUtils.looksLikeRomanNumeral(categoryLevel)) {
			return sequenceTranslator.translateRomanNumeralToSequence(categoryLevel);
		} else {
			return sequenceTranslator.translateLetterToSequence(categoryLevel);
		}
	}

	public String getReference() {
		return CfrSectionTracker.parseReferenceFromHeading(getHeading());
	}

	public String getTitleText() {
		return this.sectionTracker.getTitleTextForReference(getReference());
	}

	public String getCategoryName() {
		return CfrSectionTracker.parseCategoryTypeFromReference(getReference());
	}

	public String getCategoryLevel() {
		return CfrSectionTracker.parseCategoryLevelFromReference(getReference());
	}

	public String getFullUniqueSourceReference() {
		return this.sectionTracker.getFullUniqueSourceReferenceForReference(getReference());
	}

}
