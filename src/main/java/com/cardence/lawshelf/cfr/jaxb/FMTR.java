//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.16 at 04:22:56 PM PDT 
//


package com.cardence.lawshelf.cfr.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}TITLEPG"/>
 *         &lt;element ref="{}BTITLE"/>
 *         &lt;element ref="{}TOC"/>
 *         &lt;element ref="{}CITE"/>
 *         &lt;element ref="{}EXPLA"/>
 *         &lt;element ref="{}THISTITL"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "titlepg",
    "btitle",
    "toc",
    "cite",
    "expla",
    "thistitl"
})
@XmlRootElement(name = "FMTR")
public class FMTR {

    @XmlElement(name = "TITLEPG", required = true)
    protected TITLEPG titlepg;
    @XmlElement(name = "BTITLE", required = true)
    protected BTITLE btitle;
    @XmlElement(name = "TOC", required = true)
    protected TOC toc;
    @XmlElement(name = "CITE", required = true)
    protected CITE cite;
    @XmlElement(name = "EXPLA", required = true)
    protected EXPLA expla;
    @XmlElement(name = "THISTITL", required = true)
    protected THISTITL thistitl;

    /**
     * Gets the value of the titlepg property.
     * 
     * @return
     *     possible object is
     *     {@link TITLEPG }
     *     
     */
    public TITLEPG getTITLEPG() {
        return titlepg;
    }

    /**
     * Sets the value of the titlepg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TITLEPG }
     *     
     */
    public void setTITLEPG(TITLEPG value) {
        this.titlepg = value;
    }

    /**
     * Gets the value of the btitle property.
     * 
     * @return
     *     possible object is
     *     {@link BTITLE }
     *     
     */
    public BTITLE getBTITLE() {
        return btitle;
    }

    /**
     * Sets the value of the btitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link BTITLE }
     *     
     */
    public void setBTITLE(BTITLE value) {
        this.btitle = value;
    }

    /**
     * Gets the value of the toc property.
     * 
     * @return
     *     possible object is
     *     {@link TOC }
     *     
     */
    public TOC getTOC() {
        return toc;
    }

    /**
     * Sets the value of the toc property.
     * 
     * @param value
     *     allowed object is
     *     {@link TOC }
     *     
     */
    public void setTOC(TOC value) {
        this.toc = value;
    }

    /**
     * Gets the value of the cite property.
     * 
     * @return
     *     possible object is
     *     {@link CITE }
     *     
     */
    public CITE getCITE() {
        return cite;
    }

    /**
     * Sets the value of the cite property.
     * 
     * @param value
     *     allowed object is
     *     {@link CITE }
     *     
     */
    public void setCITE(CITE value) {
        this.cite = value;
    }

    /**
     * Gets the value of the expla property.
     * 
     * @return
     *     possible object is
     *     {@link EXPLA }
     *     
     */
    public EXPLA getEXPLA() {
        return expla;
    }

    /**
     * Sets the value of the expla property.
     * 
     * @param value
     *     allowed object is
     *     {@link EXPLA }
     *     
     */
    public void setEXPLA(EXPLA value) {
        this.expla = value;
    }

    /**
     * Gets the value of the thistitl property.
     * 
     * @return
     *     possible object is
     *     {@link THISTITL }
     *     
     */
    public THISTITL getTHISTITL() {
        return thistitl;
    }

    /**
     * Sets the value of the thistitl property.
     * 
     * @param value
     *     allowed object is
     *     {@link THISTITL }
     *     
     */
    public void setTHISTITL(THISTITL value) {
        this.thistitl = value;
    }

}
