//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.16 at 04:22:56 PM PDT 
//


package com.cardence.lawshelf.cfr.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element ref="{}SECTION" maxOccurs="unbounded"/>
 *         &lt;element ref="{}SECTNO"/>
 *         &lt;element ref="{}SUBJECT"/>
 *         &lt;element ref="{}SECHD"/>
 *         &lt;element ref="{}PRTPAGE"/>
 *         &lt;element ref="{}APP" maxOccurs="unbounded"/>
 *         &lt;element ref="{}HD"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sectionOrSECTNOOrSUBJECT"
})
@XmlRootElement(name = "SUBJGRP")
public class SUBJGRP {

    @XmlElementRefs({
        @XmlElementRef(name = "SECTION", type = SECTION.class),
        @XmlElementRef(name = "APP", type = APP.class),
        @XmlElementRef(name = "SECHD", type = JAXBElement.class),
        @XmlElementRef(name = "HD", type = HD.class),
        @XmlElementRef(name = "SUBJECT", type = SUBJECT.class),
        @XmlElementRef(name = "PRTPAGE", type = PRTPAGE.class),
        @XmlElementRef(name = "SECTNO", type = JAXBElement.class)
    })
    protected List<Object> sectionOrSECTNOOrSUBJECT;

    /**
     * Gets the value of the sectionOrSECTNOOrSUBJECT property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sectionOrSECTNOOrSUBJECT property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSECTIONOrSECTNOOrSUBJECT().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SECTION }
     * {@link APP }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link HD }
     * {@link SUBJECT }
     * {@link PRTPAGE }
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<Object> getSECTIONOrSECTNOOrSUBJECT() {
        if (sectionOrSECTNOOrSUBJECT == null) {
            sectionOrSECTNOOrSUBJECT = new ArrayList<Object>();
        }
        return this.sectionOrSECTNOOrSUBJECT;
    }

}
