package com.cardence.lawshelf.cfr.adapters;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.cardence.lawshelf.cfr.jaxb.CFRDOC;

public class CfrCodeCollectionYearExtractor {

	public static final String[] PARSE_PATTERNS = new String[] {"MMMMMMMMM dd, yyyy", "MMM. dd, yyyy"};
	private Calendar amdDate;

	public CfrCodeCollectionYearExtractor(CFRDOC cfr) {
		initWithAmdDate(cfr.getAMDDATE());
	}

	private void initWithAmdDate(String amdDate) {
		if (StringUtils.isBlank(amdDate)) {
			return;
		}
		try {
			Date parsedDate = DateUtils.parseDate(amdDate, PARSE_PATTERNS);
			if (parsedDate != null) {
				this.amdDate = GregorianCalendar.getInstance();
				this.amdDate.setTime(parsedDate);
			}
		} catch (ParseException e) {
			System.out.println("Could not parse Date: " + amdDate);
		}
	}

	public Integer getYear() {
		return this.amdDate.get(Calendar.YEAR);
	}
}
