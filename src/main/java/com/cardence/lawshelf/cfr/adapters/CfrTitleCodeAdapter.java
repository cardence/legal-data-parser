package com.cardence.lawshelf.cfr.adapters;

import com.cardence.lawshelf.cfr.CfrBaseCode;
import com.cardence.lawshelf.cfr.CfrContentContainer;
import com.cardence.lawshelf.cfr.jaxb.TITLE;

public class CfrTitleCodeAdapter extends CfrBaseCode {

	public CfrTitleCodeAdapter(TITLE title) {
		super(findHeading(title));
	}

	private static String findHeading(TITLE title) {
		CfrContentContainer hd = new CfrContentContainer();
		hd = findTitleText(hd, title);
		return hd.getContentText();
	}

	private static CfrContentContainer findTitleText(CfrContentContainer hd, TITLE title) {
		try {
			hd.addContent(title.getCFRTITLE().getTITLEHD().getHD());
		} catch (Exception e) {
			hd.addContent(title.getLRH());
		}
		return hd;
	}
}
