package com.cardence.lawshelf.cfr.adapters;

import java.util.List;

import com.cardence.lawshelf.cfr.CfrBaseSection;
import com.cardence.lawshelf.cfr.CfrContentContainer;
import com.cardence.lawshelf.cfr.CfrSectionTracker;
import com.cardence.lawshelf.cfr.CfrTags;
import com.cardence.lawshelf.cfr.jaxb.HD;
import com.cardence.lawshelf.cfr.jaxb.SUBPART;
import com.cardence.lawshelf.model.helper.SequenceTranslator;

public class CfrSubPartSectionAdapter extends CfrBaseSection {

	private int sequenceFromCounting;

	public CfrSubPartSectionAdapter(SUBPART subPart, int sequenceFromCounting) {
		super(findHeading(subPart), null);
		this.sequenceFromCounting = sequenceFromCounting;
	}

	private static String findHeading(SUBPART subPart) {
		final CfrContentContainer content = new CfrContentContainer();
		final List<Object> unknownObjects = subPart.getSECTIONOrSECTNOOrSUBJECT();
		final HD hd = findHD(unknownObjects);
		if (hd == null) {
			final String alternativeText = findAlternativeToHD(unknownObjects);
			if (alternativeText == null) {
				throw new RuntimeException("Could not find a header text for subpart");
			}
			content.addContent(alternativeText);
		} else {
			content.addContent(hd);
		}

		return content.getContentText();
	}

	@Override
	public String getFullUniqueSourceReference() {
		try {
			return super.getFullUniqueSourceReference();
		} catch (Exception e) {
			return this.sectionTracker.getFullUniqueSourceReferenceForReferenceAndCategoryLevel(getReference(),
					getCategoryLevel());
		}
	}

	@Override
	public String getCategoryName() {
		try {
			return super.getCategoryName();
		} catch (Exception e) {
			return CfrTags.LEVEL_TYPE_SUBPART;
		}
	}

	@Override
	public String getCategoryLevel() {
		try {
			return super.getCategoryLevel();
		} catch (Exception e) {
			return translateSequenceFromCountingIntoCategoryLevel();
		}
	}

	@Override
	public String getReference() {
		try {
			String ref = super.getReference();

			try {
				 CfrSectionTracker.parseCategoryLevelFromReference(ref);
				return ref;
			} catch (Exception e) {
				return customBuildSubPartReferenceUsingSequence();
			}
		} catch (Exception e) {
			return customBuildSubPartReferenceUsingSequence();
		}
	}

	private String customBuildSubPartReferenceUsingSequence() {
		return CfrTags.LEVEL_TYPE_SUBPART + " " + translateSequenceFromCountingIntoCategoryLevel();
	}

	private String translateSequenceFromCountingIntoCategoryLevel() {
		return SequenceTranslator.getInstance().translateSequenceToLetter(this.sequenceFromCounting);
	}

}
