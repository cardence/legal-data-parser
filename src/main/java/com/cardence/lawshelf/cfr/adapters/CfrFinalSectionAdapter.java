package com.cardence.lawshelf.cfr.adapters;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;

import com.cardence.lawshelf.cfr.CfrBaseSection;
import com.cardence.lawshelf.cfr.CfrContentContainer;
import com.cardence.lawshelf.cfr.jaxb.SECTION;
import com.cardence.lawshelf.cfr.jaxb.SUBJECT;

@SuppressWarnings("restriction")
public final class CfrFinalSectionAdapter extends CfrBaseSection {

	private String reference;
	private String titleText;
	private String categoryLevel;
	private SECTION section;

	public CfrFinalSectionAdapter(SECTION section) {
		this(section, null);
	}

	public CfrFinalSectionAdapter(SECTION section, String parentReference) {
		super("", parentReference);
		this.section = section;
	}

	public String getHeading() {
		return getReference() + " - " + getTitleText();
	}

	public String getReference() {
		if (this.reference == null) {
			this.reference = getSectionReferenceFromSection();
		}
		return this.reference;
	}

	public String getTitleText() {
		if (this.titleText == null) {
			this.titleText = getSectionTitleTextFromSection();
		}
		return this.titleText;
	}

	public String getCategoryName() {
		return "SECTION";
	}

	public String getCategoryLevel() {
		if (this.categoryLevel == null) {
			this.categoryLevel = parseCategoryLevelFromReference();
		}
		return this.categoryLevel;
	}

	public String getFullUniqueSourceReference() {
		final String parentReference = this.sectionTracker
				.getFullUniqueSourceReferenceForReference(getParentReference());
		return (this.sectionTracker.appendCategoryLevelToSourceReference(getCategoryLevel(), parentReference));
	}

	@SuppressWarnings("restriction")
	private String getSectionReferenceFromSection() {
		for (Object obj : section.getSUBJECTOrAUTHOrAPPRO()) {
			if (obj instanceof JAXBElement) {
				@SuppressWarnings("rawtypes")
				JAXBElement jaxb = (JAXBElement) obj;
				if (!jaxb.isNil() && StringUtils.equals("SECTNO", jaxb.getName().getLocalPart())) {
					return (String) jaxb.getValue();
				}
			}
		}
		return null;
	}

	@SuppressWarnings("restriction")
	private String getSectionTitleTextFromSection() {
		for (Object obj : section.getSUBJECTOrAUTHOrAPPRO()) {
			if (obj instanceof SUBJECT) {
				final SUBJECT subject = (SUBJECT) obj;
				CfrContentContainer content = new CfrContentContainer();
				content.addContentList(subject.getContent());
				return content.getContentText();
			}
		}
		for (Object obj : section.getSUBJECTOrAUTHOrAPPRO()) {
			if (obj instanceof JAXBElement) {
				final JAXBElement jaxb = (JAXBElement) obj;
				if (StringUtils.equals("RESERVED", jaxb.getName().getLocalPart())) {
					CfrContentContainer content = new CfrContentContainer();
					content.addContent(jaxb.getValue().toString());
					return content.getContentText();
				}
			}
		}
		for (Object obj : section.getSUBJECTOrAUTHOrAPPRO()) {
			if (obj instanceof String) {
				CfrContentContainer content = new CfrContentContainer();
				content.addContent((String) obj);
				return content.getContentText();
			}
		}
		return null;
	}

	private String parseCategoryLevelFromReference() {
		final String reference = getReference();
		return StringUtils.substringAfterLast(reference, ".");
	}
}
