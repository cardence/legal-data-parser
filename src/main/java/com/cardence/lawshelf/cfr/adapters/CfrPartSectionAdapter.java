package com.cardence.lawshelf.cfr.adapters;

import java.util.List;

import com.cardence.lawshelf.cfr.CfrBaseSection;
import com.cardence.lawshelf.cfr.CfrContentContainer;
import com.cardence.lawshelf.cfr.jaxb.HD;
import com.cardence.lawshelf.cfr.jaxb.PART;
import com.cardence.lawshelf.cfr.jaxb.SUBPART;

public class CfrPartSectionAdapter extends CfrBaseSection {

	public CfrPartSectionAdapter(PART part) {
		super(findHeading(part), null);
	}


	private static String findHeading(PART part){
		final CfrContentContainer content = new CfrContentContainer();
		final List<Object> unknownObjects = part.getContent();
		final HD hd = findHD(unknownObjects);
		if (hd == null) {
			final String alternativeText = findAlternativeToHD(unknownObjects);
			if (alternativeText == null) {
				throw new RuntimeException("Could not find a header text for part");
			}
			content.addContent(alternativeText);
		} else {
			content.addContent(hd);
		}

		return content.getContentText();
	}

}
