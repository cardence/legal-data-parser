package com.cardence.lawshelf.cfr.adapters;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.cardence.lawshelf.cfr.CfrBaseSection;
import com.cardence.lawshelf.cfr.CfrContentContainer;
import com.cardence.lawshelf.cfr.jaxb.CHAPTER;
import com.cardence.lawshelf.cfr.jaxb.HD;
import com.cardence.lawshelf.cfr.jaxb.TOC;

public class CfrChapterSectionAdapter extends CfrBaseSection {

	public CfrChapterSectionAdapter(CHAPTER chapter) {
		super(findHeading(chapter), null);
	}

	public CfrChapterSectionAdapter(CHAPTER chapter, String parentReference) {
		super(findHeading(chapter), parentReference);
	}

	private static String findHeading(CHAPTER chapter) {
		final CfrContentContainer content = new CfrContentContainer();
		final List<Object> unknownObjects = chapter.getContent();
		final HD hd = findHD(unknownObjects);
		if (hd == null) {
			final String alternativeText = findAlternativeToHD(unknownObjects);
			if (alternativeText == null) {
				throw new RuntimeException("Could not find a header text for chapter");
			}
			content.addContent(alternativeText);
		} else {
			content.addContent(hd);
		}

		return content.getContentText();
	}

	protected static HD findHD(List<Object> unknownObjects) {
		List<HD> candidates = getCandidateHDs(unknownObjects);
		if (candidates != null) {
			return determineBestCanddiateHD(candidates);
		}
		return null;
	}

	private static HD determineBestCanddiateHD(List<HD> candidates) {
		for (HD hd : candidates) {
			final CfrContentContainer content = new CfrContentContainer();
			content.addContent(hd);
			if (StringUtils.isNotBlank(content.getContentText())) {
				return hd;
			}
		}
		return null;
	}

	private static List<HD> getCandidateHDs(List<Object> unknownObjects) {
		List<HD> candidates = new ArrayList<HD>();

		for (Object unknown : unknownObjects) {
			if (unknown instanceof HD) {
				candidates.add(((HD) unknown));
			} else if (unknown instanceof TOC) {
				TOC toc = (TOC) unknown;
				candidates.add(toc.getTOCHD().getHD());
			}
		}
		return candidates;
	}
}
