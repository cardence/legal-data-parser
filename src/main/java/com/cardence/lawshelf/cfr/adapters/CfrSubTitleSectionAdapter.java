package com.cardence.lawshelf.cfr.adapters;

import com.cardence.lawshelf.cfr.CfrBaseSection;
import com.cardence.lawshelf.cfr.CfrContentContainer;
import com.cardence.lawshelf.cfr.jaxb.HD;
import com.cardence.lawshelf.cfr.jaxb.SUBTITLE;

public class CfrSubTitleSectionAdapter extends CfrBaseSection {

	public CfrSubTitleSectionAdapter(SUBTITLE subTitle) {
		super(findHeading(subTitle), null);
	}

	public CfrSubTitleSectionAdapter(SUBTITLE subTitle, String parentReference) {
		super(findHeading(subTitle), parentReference);
	}

	private static String findHeading(SUBTITLE subTitle) {
		final CfrContentContainer content = new CfrContentContainer();
		final HD hd = subTitle.getHD();
		if (hd == null) {
			final String alternativeText = subTitle.getRESERVED();
			if (alternativeText == null) {
				throw new RuntimeException("Could not find a header text for subtitle");
			}
			content.addContent(alternativeText);
		} else {
			content.addContent(hd);
		}

		return content.getContentText();
	}

}
