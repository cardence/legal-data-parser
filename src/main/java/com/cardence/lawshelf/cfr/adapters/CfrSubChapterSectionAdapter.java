package com.cardence.lawshelf.cfr.adapters;

import java.util.List;

import com.cardence.lawshelf.cfr.CfrBaseSection;
import com.cardence.lawshelf.cfr.CfrContentContainer;
import com.cardence.lawshelf.cfr.jaxb.HD;
import com.cardence.lawshelf.cfr.jaxb.SUBCHAP;

public class CfrSubChapterSectionAdapter extends CfrBaseSection {

	public CfrSubChapterSectionAdapter(SUBCHAP subChapter) {
		super(findHeading(subChapter), null);
	}

	private static String findHeading(SUBCHAP subChapter) {
		final CfrContentContainer content = new CfrContentContainer();
		final List<Object> unknownObjects = subChapter.getPRTPAGEOrRESERVEDOrHD();
		final HD hd = findHD(unknownObjects);
		if (hd == null) {
			final String alternativeText = findAlternativeToHD(unknownObjects);
			if (alternativeText == null) {
				throw new RuntimeException("Could not find a header text for subchapter");
			}
			content.addContent(alternativeText);
		} else {
			content.addContent(hd);
		}

		return content.getContentText();
	}
}
