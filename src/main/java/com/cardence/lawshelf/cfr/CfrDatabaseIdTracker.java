package com.cardence.lawshelf.cfr;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_SINGLETON)
public class CfrDatabaseIdTracker {

	private Map<String, Integer> referenceMapToDatabaseIds;
	private Map<Integer, String> databaseIdMapToReference;

	@Autowired
	private static CfrDatabaseIdTracker me;

	public static CfrDatabaseIdTracker getInstance() {
		return me;
	}

	public void linkDatabaseIdToReference(Integer databaseId, String reference) {
		this.getReferenceMapToDatabaseIds().put(reference, databaseId);
		this.getDatabaseIdMapToReference().put(databaseId, reference);
	}

	public Integer getDatabaseIdForReference(String reference) {
		return this.getReferenceMapToDatabaseIds().get(reference);
	}

	public String getReferenceForDatabaseId(Integer databaseId) {
		return this.getDatabaseIdMapToReference().get(databaseId);
	}

	private Map<String, Integer> getReferenceMapToDatabaseIds() {
		if (this.referenceMapToDatabaseIds == null) {
			this.referenceMapToDatabaseIds = new HashMap<String, Integer>();
		}
		return this.referenceMapToDatabaseIds;
	}

	private Map<Integer, String> getDatabaseIdMapToReference() {
		if (this.databaseIdMapToReference == null) {
			this.databaseIdMapToReference = new HashMap<Integer, String>();
		}
		return this.databaseIdMapToReference;
	}

}
