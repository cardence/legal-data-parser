package com.cardence.lawshelf.cfr;

public interface CfrSection {

	String getHeading();
	String getReference();
	String getTitleText();
	String getCategoryName();
	String getCategoryLevel();
	String getFullUniqueSourceReference();
	int getSequence();
}
