package com.cardence.lawshelf.cfr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.support.LocalizedResourceHelper;

import com.cardence.lawshelf.cfr.jaxb.APPENDIX;
import com.cardence.lawshelf.cfr.jaxb.AUTH;
import com.cardence.lawshelf.cfr.jaxb.CITA;
import com.cardence.lawshelf.cfr.jaxb.CONTENTS;
import com.cardence.lawshelf.cfr.jaxb.CROSSREF;
import com.cardence.lawshelf.cfr.jaxb.E;
import com.cardence.lawshelf.cfr.jaxb.EDNOTE;
import com.cardence.lawshelf.cfr.jaxb.EFFDNOT;
import com.cardence.lawshelf.cfr.jaxb.EXTRACT;
import com.cardence.lawshelf.cfr.jaxb.FP;
import com.cardence.lawshelf.cfr.jaxb.FTNT;
import com.cardence.lawshelf.cfr.jaxb.FTREF;
import com.cardence.lawshelf.cfr.jaxb.GPOTABLE;
import com.cardence.lawshelf.cfr.jaxb.HD;
import com.cardence.lawshelf.cfr.jaxb.NOTE;
import com.cardence.lawshelf.cfr.jaxb.P;
import com.cardence.lawshelf.cfr.jaxb.PRTPAGE;
import com.cardence.lawshelf.cfr.jaxb.SOURCE;
import com.cardence.lawshelf.cfr.jaxb.SUBJGRP;

public class CfrContentContainer {

	private Log log = LogFactory.getLog(CfrContentContainer.class);

	public enum Tag {
		P, DIV, SPAN
	};

	public enum Style {
		BOLD, ITALIC, UNDERLINE, SMALLCAPS, UPPERCASE, LOWERCASE, BORDER_BOTTOM, BORDER_TOP, BORDER_LEFT, BORDER_RIGHT, SIZE_PLUS_1, SIZE_MINUS_1, LEFT, CENTER, RIGHT, WIDTH_100P
	};

	private List<Object> contents;

	// private StringBuffer contentBuffer = new StringBuffer();

	private Tag tag;
	private List<Style> styles;

	public CfrContentContainer() {
	}

	public CfrContentContainer(Tag tag) {
		super();
		this.tag = tag;
	}

	public CfrContentContainer(Tag tag, Style... styles) {
		super();
		this.tag = tag;
		addStyles(styles);
	}

	public void addStyles(Style... styles) {
		this.getStyles().addAll(Arrays.asList(styles));
	}

	public String getStyledContent() {
		StringBuffer sb = new StringBuffer();
		for (Object obj : getContents()) {
			if (obj instanceof String) {
				sb.append((String) obj);
			} else if (obj instanceof StringBuffer) {
				sb.append((StringBuffer) obj);
			} else if (obj instanceof CfrContentContainer) {
				sb.append(((CfrContentContainer) obj).getStyledContent());
			}
		}
		return wrapInTag(sb.toString());
	}

	public String getContentText() {
		StringBuffer sb = new StringBuffer();
		for (Object obj : getContents()) {
			if (obj instanceof String) {
				sb.append((String) obj);
			} else if (obj instanceof StringBuffer) {
				sb.append((StringBuffer) obj);
			} else if (obj instanceof CfrContentContainer) {
				sb.append(((CfrContentContainer) obj).getContentText());
			}
		}
		return StringUtils.trimToEmpty(sb.toString());
	}

	public Tag getTag() {
		return this.tag;
	}

	public List<Style> getStyles() {
		if (this.styles == null) {
			this.styles = new ArrayList<Style>();
		}
		return this.styles;
	}

	protected final List<Object> getContents() {
		if (this.contents == null) {
			this.contents = new ArrayList<Object>();
		}
		return this.contents;
	}

	private String buildStyleValueForTag() {
		final List<Style> styles = getStyles();
		if (styles == null || styles.size() == 0) {
			return "";
		} else {
			final StringBuffer sb = new StringBuffer("style='");
			for (Style style : styles) {
				sb.append(getStyleStringForStyle(style));
			}
			sb.append("'");
			return sb.toString();
		}
	}

	private String getStyleStringForStyle(Style style) {

		switch (style) {
		case BOLD:
			return "font-weight: bold;";
		case ITALIC:
			return "font-style: italic;";
		case SMALLCAPS:
			return "font-variant: small-caps;";
		case LOWERCASE:
			return "text-transform: lowercase;";
		case UPPERCASE:
			return "text-transform: uppercase;";
		case BORDER_BOTTOM:
			return "border-bottom: medium solid #000000;";
		case BORDER_TOP:
			return "border-top: medium solid #000000;";
		case BORDER_LEFT:
			return "border-left: medium solid #000000;";
		case BORDER_RIGHT:
			return "border-right: medium solid #000000;";
		case SIZE_PLUS_1:
			return "font-size: large;";
		case SIZE_MINUS_1:
			return "font-size: small;";
		case LEFT:
			return "text-align: left;";
		case CENTER:
			return "text-align: center;";
		case RIGHT:
			return "text-align: right;";
		case WIDTH_100P:
			return "width: 100px;";
		default:
			return "";
		}
	}

	private String wrapInTag(String content) {
		final String tagname = tag.name().toLowerCase();
		String styleAttribute = buildStyleValueForTag();
		styleAttribute = (StringUtils.isBlank(styleAttribute)) ? "" : " " + styleAttribute;

		return "<" + tagname + styleAttribute + ">" + StringUtils.trimToEmpty(content) + "</" + tagname + ">";
	}

	public void addContent(P content) {
		if (content == null) {
			return;
		}
		// main content
		String source = content.getSOURCE();

		CfrContentContainer cont = new CfrContentContainer(Tag.P);
		if (source == null) {
		} else {
			log.info("Unknown source for P: " + source);
		}

		cont.addContentList(content.getContent());
		this.addContent(cont);
	}

	public void addContent(FP content) {
		if (content == null) {
			return;
		}
		// TODO: FP
		// CONTENT (flush paragraph)
		// IF ATTRIBUTE SOURCE = "FP-DASH", then fill in a bunch of dashes...

		// maybe make the DIV have an underline?

		String src = content.getSOURCE();
		if (src != null && StringUtils.equals(src, "FP-DASH")) {

		}

		List<Object> list = content.getContent();

		for (Object o : list) {
			if (o instanceof E) {

			} else if (o instanceof E) {

			} else if (o instanceof FTREF) {

			} else if (o instanceof PRTPAGE) {

			} else if (o instanceof String) {
				// content
			} else if (o instanceof JAXBElement) {
				// content
			}
		}

	}

	
	public void addContent(HD content) {
		if (content == null) {
			return;
		}
		// title
		String source = content.getSOURCE();

		CfrContentContainer cont = new CfrContentContainer(Tag.P);
		if (source == null) {
		} else if ("HED".equals(source)) {
			cont.addStyles(Style.BOLD);
		} else if ("H1".equals(source)) {
			cont.addStyles(Style.SMALLCAPS);
		} else if ("H2".equals(source)) {
			cont.addStyles(Style.CENTER, Style.SMALLCAPS);
		} else if ("H3".equals(source)) {
			cont.addStyles(Style.CENTER);
		} else if ("H4".equals(source)) {
			cont.addStyles(Style.CENTER);
		} else {
			cont.addStyles(Style.BOLD);
		}

		cont.addContentList(content.getContent());
		this.addContent(cont);
	}

	public void addContent(String content) {
		if (content == null) {
			return;
		}
		// plain string
		this.getContents().add(content);
	}

	public void addContent(StringBuffer content) {
		if (content == null) {
			return;
		}
		// plain string
		this.getContents().add(content);
	}

	public void addContent(E content) {
		if (content == null) {
			return;
		}
		// text.... not sure if to bold or not
		String t = content.getT();
		CfrContentContainer cont = new CfrContentContainer(Tag.SPAN);
		if (t == null) {
		} else if ("01".equals(t)) {
			cont.addStyles(Style.BOLD);
		} else if ("02".equals(t)) {
			cont.addStyles(Style.BOLD, Style.SMALLCAPS);
		} else if ("03".equals(t)) {
			cont.addStyles(Style.ITALIC);
		} else if ("04".equals(t)) {
			cont.addStyles(Style.SMALLCAPS);
		} else if ("05".equals(t)) {
			cont.addStyles(Style.SMALLCAPS);
		} else {
			cont.addStyles(Style.BOLD);
		}

		cont.addContentList(content.getContent());
		this.addContent(cont);
	}

	public void addContent(PRTPAGE content) {
		if (content == null) {
			return;
		}
		// ignore
		ignore(content);
	}

	public void addContent(GPOTABLE content) {
		if (content == null) {
			return;
		}
		// TODO: TABLE

	}

	public void addContent(AUTH content) {
		if (content == null) {
			return;
		}
		// note : authority
		// TODO: AUTH
	}

	public void addContent(CITA content) {
		if (content == null) {
			return;
		}
		// note : citations

		// TODO: CITA
	}

	public void addContent(NOTE content) {
		if (content == null) {
			return;
		}
		// note container: can have HD & P

		// TODO: NOTE
	}

	public void addContent(EXTRACT content) {
		if (content == null) {
			return;
		}
		// content : container -> *HD + *P

		// TODO: EXTRACT
	}

	public void addContent(FTNT content) {
		if (content == null) {
			return;
		}
		// note : footnote -> container for another p

		// TODO: FTNT
	}

	public void addContent(JAXBElement<?> content) {
		if (content == null) {
			return;
		}
		// content

		// TODO: JAXBElement
	}

	public void addContent(CROSSREF content) {
		if (content == null) {
			return;
		}
		// note : cross reference container - HD and P

		// TODO: CROSSREF
	}

	public void addContent(EDNOTE content) {
		if (content == null) {
			return;
		}
		// note : end note container - HD and P

		// TODO: EDNOTE
	}

	public void addContent(SOURCE content) {
		if (content == null) {
			return;
		}
		// note

		// TODO: SOURCE
	}

	public void addContent(CfrContentContainer content) {
		if (content == null) {
			return;
		}

		this.getContents().add(content);
		// ???

		// TODO: CfrContentContainer
	}

	public void addContentList(List<Object> contentlist) {
		if (contentlist == null) {
			return;
		}
		for (Object o : contentlist) {
			this.addContentObject(o);
		}
	}

	private void addContent(Object o) {
		if (o == null) {
			log.warn("Content is null");
		} else {
			log.warn("Found unknown content: " + o.getClass().getName());
		}
	}

	public void addContentObject(Object content) {
		if (content == null) {
			return;
		}
		final Object o = content;

		if (o instanceof String) {
			addContent((String) o);
		} else if (o instanceof HD) {
			// title
			addContent((HD) content);
		} else if (o instanceof E) {
			// emphasis content
			addContent((E) content);
		} else if (o instanceof GPOTABLE) {
			// table
			addContent((GPOTABLE) content);
		} else if (o instanceof P) {
			// CONTENT
			addContent((P) content);
		} else if (o instanceof NOTE) {
			// note container: can have HD & P
			addContent((NOTE) content);
		} else if (o instanceof FP) {
			// CONTENT (flush paragraph)
			// IF ATTRIBUTE SOURCE = "FP-DASH", then fill in a bunch of
			// dashes... maybe make the DIV have an underline?
			addContent((FP) content);
		} else if (o instanceof CITA) {
			// note : citations
			addContent((CITA) content);
		} else if (o instanceof AUTH) {
			// note : authority
			addContent((AUTH) content);
		} else if (o instanceof PRTPAGE) {
			// ignore
			addContent((PRTPAGE) content);
		} else if (o instanceof SOURCE) {
			// note
			addContent((SOURCE) content);
		} else if (o instanceof CONTENTS) {
			// toc : ignore
			ignore(content);
		} else if (o instanceof EDNOTE) {
			// note : end note container - HD and P
			addContent((EDNOTE) content);
		} else if (o instanceof SUBJGRP) {
			// can appear in a list of sections to "group" them.
			// we currently have no way to store this.
			// it would be nice to have a section inside a uitableview
			// oh well, we'll just make this a type and nest it down a level.
			// has a HD and more sections.. nothing more really
			addContent((SUBJGRP) content);
		} else if (o instanceof CROSSREF) {
			// note : cross reference container - HD and P
			addContent((CROSSREF) content);
		} else if (o instanceof JAXBElement<?>) {
			// content
			addContent((JAXBElement<?>) content);
		} else if (o instanceof EFFDNOT) {
			// not sure
			addContent((EFFDNOT) content);
		} else if (o instanceof FTNT) {
			// note : footnote -> container for another p
			addContent((FTNT) content);
		} else if (o instanceof EXTRACT) {
			// content : container -> *HD + *P
			addContent((EXTRACT) content);
		} else if (o instanceof APPENDIX) {
			// SAME LEVEL AS SECTION OR PROBABLY ANY OTHER TYPE
			addContent((APPENDIX) content);
		}
	}

	private void ignore(Object content) {

	}
}