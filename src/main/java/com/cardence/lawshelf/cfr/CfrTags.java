package com.cardence.lawshelf.cfr;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public interface CfrTags {


	String LEVEL_TYPE_TITLE = "TITLE";
	String LEVEL_TYPE_SECTION = "SECTION";
	String LEVEL_TYPE_CHAPTER = "CHAPTER";
	String LEVEL_TYPE_SUBCHAPTER = "SUBCHAPTER";
	String LEVEL_TYPE_PART = "PART";
	String LEVEL_TYPE_SUBPART = "SUBPART";
	String LEVEL_TYPE_RULE = "RULE";

	Set<String> LEVEL_TYPE_SECTION_MATCH = new HashSet<String>(Arrays.asList(
			"SEC.", "SECS."));

	Set<String> LEVEL_TYPE_WITH_ROMAN_NUMERALS = new HashSet<String>(
			Arrays.asList(LEVEL_TYPE_CHAPTER));

}
