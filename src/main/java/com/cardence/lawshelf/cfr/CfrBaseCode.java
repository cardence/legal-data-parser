package com.cardence.lawshelf.cfr;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import com.cardence.lawshelf.cfr.jaxb.HD;
import com.cardence.lawshelf.model.helper.RomanNumeralUtils;
import com.cardence.lawshelf.model.helper.SequenceTranslator;

public class CfrBaseCode implements CfrCode {

	private CfrSectionTracker sectionTracker = CfrSectionTracker.getInstance();

	// private CfrDatabaseIdTracker databaseIdTracker =
	// CfrDatabaseIdTracker.getInstance();

	private SequenceTranslator sequenceTranslator = SequenceTranslator.getInstance();

	private String heading;

	protected CfrBaseCode(String heading) {
		this.heading = heading;
	}

	public static final CfrBaseCode getCfrCodeForHeadingAndParentReference(String heading) {
		return new CfrBaseCode(heading);
	}

	protected static HD findHD(List<Object> unknownObjects) {
		for (Object unknown : unknownObjects) {
			if (unknown instanceof HD) {
				return ((HD) unknown);
			}
		}
		return null;
	}

	public String getName() {
		return CfrSectionTracker.parseTitleFromHeading(getHeading());
	}

	public String getHeading() {
		return this.heading;
	}

	public String getShortHeading() {
		return this.sectionTracker.getHeadingForReference(getReference());
	}

	public int getCodeSequence() {
		final String categoryLevel = CfrSectionTracker.parseCategoryLevelFromReference(getReference());
		if (NumberUtils.isDigits(categoryLevel)) {
			return NumberUtils.toInt(categoryLevel);
		} else if (RomanNumeralUtils.looksLikeRomanNumeral(categoryLevel)) {
			return sequenceTranslator.translateRomanNumeralToSequence(categoryLevel);
		} else {
			return sequenceTranslator.translateLetterToSequence(categoryLevel);
		}
	}

	public String getStatus() {
		return "active";
	}

	public String getTitle() {
		return getReference();
	}

	public String getReference() {
		return CfrSectionTracker.parseReferenceFromHeading(getHeading());
	}

}
