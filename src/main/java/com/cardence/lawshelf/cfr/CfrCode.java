package com.cardence.lawshelf.cfr;

public interface CfrCode {

	
	 String getName();
	 String getHeading();
	 String getShortHeading();
	 int getCodeSequence();
	 String getStatus();
	 String getTitle(); // reference
	 String getReference(); // reference
}
