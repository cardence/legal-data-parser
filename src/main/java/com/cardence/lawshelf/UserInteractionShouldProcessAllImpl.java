package com.cardence.lawshelf;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserInteractionShouldProcessAllImpl extends UserInteractionImpl
		implements UserInteractionShouldProcessAll {

	@Autowired
	private Log log;

	public boolean getShouldProcessAll() {

		// create the choices
		UserChoicePresenter yesOrNo = new UserChoicePresenter() {
			public void printChoices() {
				System.out.println("[Y] Yes");
				System.out.println("[N] No");
			}
		};

		// ask the question
		String answer = super.askQuestion("Do you wish to process all?",
				yesOrNo);
		System.out.println("you chose [" + answer + "]");

		if (answer == null || !answer.trim().toUpperCase().equals("Y")) {
			return false;
		} else {
			return true;
		}
	}

}
