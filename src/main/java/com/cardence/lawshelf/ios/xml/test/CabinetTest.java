package com.cardence.lawshelf.ios.xml.test;

import static org.junit.Assert.fail;

import java.io.File;
import java.math.BigInteger;
import java.util.Date;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.junit.Before;
import org.junit.Test;

import com.cardence.lawshelf.ios.xml.Cabinet;
import com.cardence.lawshelf.ios.xml.Folder;
import com.cardence.lawshelf.ios.xml.ObjectFactory;

public class CabinetTest {
	private static final String LI1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim condimentum nunc, at pretium diam condimentum et. Suspendisse potenti. Morbi convallis, lacus sed cursus imperdiet, velit odio dignissim nisl, vel sagittis metus dolor a ligula. Ut vehicula neque ac quam egestas interdum. Donec aliquet placerat elit, eget tempus felis molestie ac. Etiam vestibulum adipiscing mauris non fringilla. Mauris ante sem, fermentum eu pellentesque et, interdum malesuada lectus. Nulla dapibus nisi id sem sollicitudin posuere. Sed sed leo a diam faucibus aliquet at sit amet lorem. Pellentesque auctor mattis imperdiet. Mauris tempus, nunc id mattis semper, felis turpis dignissim justo, sed venenatis elit sem ut metus. Suspendisse erat leo, consectetur ut semper et, suscipit sed lacus. Maecenas vel egestas turpis. Morbi molestie lacinia pharetra. Phasellus elit erat, suscipit ac rutrum eu, dictum at mauris. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.";
	private static final String LI2 = "Maecenas vel dui quis leo varius ornare et vitae quam. Aenean facilisis nisl ut est dignissim ultricies. Curabitur blandit orci a quam egestas bibendum. Curabitur augue lectus, facilisis vitae ultrices at, eleifend non justo. Nam et tortor lorem, vel pretium quam. Duis ornare consequat ipsum a eleifend. Sed ligula neque, lacinia non mollis a, porta a ante. Phasellus suscipit sem in diam aliquam non suscipit tortor sodales. Proin at metus ac mauris pretium eleifend. Suspendisse eget tempus justo. Aenean quis dolor tellus, sed condimentum nulla. Aliquam ipsum dui, pharetra sit amet placerat vitae, ultricies ut libero. Proin posuere pulvinar suscipit. Sed suscipit suscipit ante, id dictum libero viverra in.";
	private static final String LI3 = "Vivamus hendrerit nunc urna. In venenatis libero in risus ultrices mollis. Pellentesque sed leo non lacus sollicitudin tempor id quis tellus. Suspendisse dapibus dui ac nunc cursus eget posuere justo ullamcorper. Nam et ligula ante. Donec erat velit, malesuada a fermentum in, aliquet non est. Sed lacinia felis quis erat fermentum vitae euismod dui lacinia. Donec pellentesque lobortis nibh sed interdum. Fusce sit amet eros et quam accumsan euismod at sit amet sapien. Phasellus fermentum pretium eros, a placerat diam scelerisque sed. Pellentesque a lorem vel urna convallis pretium in vel justo. Praesent bibendum, neque id condimentum porta, elit massa dictum nisi, id ultrices lorem eros at nisl. Donec commodo dignissim mattis.";
	private static final String LI4 = "Quisque in volutpat leo. Maecenas accumsan consectetur mattis. Nulla facilisi. Pellentesque justo augue, placerat non pellentesque sed, ultrices eget urna. Fusce id diam elit. Etiam luctus est ut elit luctus vestibulum eu vitae purus. Nulla et mauris dolor. Pellentesque imperdiet mollis tincidunt. In congue posuere dolor ut auctor. Donec feugiat massa ut libero interdum blandit. Quisque venenatis sodales ultrices. Curabitur elementum ultrices varius. Vestibulum pellentesque fringilla mattis. Aliquam erat volutpat. In hac habitasse platea dictumst. Vivamus nec lectus at dui interdum hendrerit sed at tellus.";
	private static final String LI5 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a leo non lacus vestibulum pulvinar. Aenean porttitor placerat libero eget commodo. Vivamus elementum dictum augue, ut vehicula eros accumsan nec. Donec accumsan lobortis est, nec vehicula lacus accumsan a. Donec et libero ante. Nulla facilisi. In hac habitasse platea dictumst.";
	private static final String LI6 = "Ut sit amet magna enim, sit amet sollicitudin dui. In.";

	private static final Random RANDOM = new Random((new Date()).getTime());
	Cabinet cabinet;

	@Before
	public void setUp() throws Exception {

		cabinet = new Cabinet();
		cabinet.setAbbr("CA");
		cabinet.setState(true);
		cabinet.setFederal(false);
		cabinet.setTitle("VEHICLE CODE");
		cabinet.setId(RANDOM.nextInt() + "");
		cabinet.setBackendID(BigInteger.valueOf(1));

		int totalFolders = RANDOM.nextInt(10) + 1;
		for (int i = 0; i < totalFolders; i++) {
			// cabinet.().add(generateFolder(i));
		}

	}

	private Folder generateFolder(int index) {
		int uIdx = index * RANDOM.nextInt(100000);
		Folder folder = new Folder();
		folder.setCategory("my test " + uIdx + " category");
		folder.setDisplay("hello display world for [" + uIdx + "]");
		folder.setId("" + RANDOM.nextInt());
		folder.setTitle("Title " + index + ". This is my most amazing title ");
		folder.setCategoryIndex(BigInteger.valueOf(index));
		folder.setBackendID(BigInteger.valueOf(uIdx));

		if (RANDOM.nextBoolean()) {
			// subfolders
			int totalFolders = RANDOM.nextInt(3) + 1;
			for (int i = 0; i < totalFolders; i++) {
				folder.getFolder().add(generateFolder(index));
			}

		} else {
			// files
			if (RANDOM.nextBoolean()) {
				folder.setFile(generateFile(index));
			} else {
				folder.setNotes(generateFile(index));
			}
		}
		return folder;
	}

	private com.cardence.lawshelf.ios.xml.Content generateFile(int index) {
		int uIdx = index * RANDOM.nextInt(100000);
		com.cardence.lawshelf.ios.xml.Content file = new com.cardence.lawshelf.ios.xml.Content();
		file.setId("" + uIdx);
		file.setValue(getContent(index));
		return file;
	}

	private String getContent(int index) {
		StringBuffer sb = new StringBuffer();

		int len = index % 3 * RANDOM.nextInt(3);
		len = (len > 0) ? len : 1;
		for (int i = 0; i < len; i++) {
			sb.append(getRandomChunk());
		}

		return sb.toString();
	}

	private String getRandomChunk() {
		switch (RANDOM.nextInt(6)) {
		case 0:
			return LI1;
		case 1:
			return LI2;
		case 2:
			return LI3;
		case 3:
			return LI4;
		case 4:
			return LI5;
		case 5:
			return LI6;
		default:
			return LI1;
		}
	}

	@Test
	public void test() {
		try {
			File file = new File("C:\\Users\\mkboudreau\\Desktop\\test.xml");

			ObjectFactory factory = new ObjectFactory();
			// JAXBElement<Cabinet> jaxbRoot = factory.createCabinet();

			JAXBContext ctx = JAXBContext.newInstance(cabinet.getClass());
			Marshaller marshaller = ctx.createMarshaller();

			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
					Boolean.TRUE);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(
					Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION,
					"LawShelfIDX.xsd");

			// marshaller.marshal(jaxbRoot, file);
		} catch (Throwable t) {
			fail(t.getLocalizedMessage());
		}

	}

}
