package com.cardence.lawshelf.ios;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cardence.lawshelf.ios.xml.Asset;
import com.cardence.lawshelf.ios.xml.Cabinet;
import com.cardence.lawshelf.ios.xml.Content;
import com.cardence.lawshelf.ios.xml.Folder;
import com.cardence.lawshelf.model.Code;
import com.cardence.lawshelf.model.CodeCollection;
import com.cardence.lawshelf.model.CodeCollectionDao;
import com.cardence.lawshelf.model.CodeDao;
import com.cardence.lawshelf.model.ContentFull;
import com.cardence.lawshelf.model.ContentFullDao;
import com.cardence.lawshelf.model.ContentPart;
import com.cardence.lawshelf.model.ContentPartDao;
import com.cardence.lawshelf.model.Section;
import com.cardence.lawshelf.model.SectionDao;

@Component
public class ConvertCodeToXml {

	@Autowired
	private Log log;

	@Autowired
	private CodeCollectionDao codeCollectionDao;

	@Autowired
	private CodeDao codeDao;

	@Autowired
	private SectionDao sectionDao;

	@Autowired
	private ContentFullDao contentFullDao;

	@Autowired
	private ContentPartDao contentPartDao;

	public Asset convertCodeWithID(Integer id) {

		Code code = codeDao.findCode(id);
		CodeCollection codeCollection = codeCollectionDao
				.findCodeCollection(code.getCodeCollectionId());

		Cabinet cabinet = convertCodeCollection(codeCollection);
		cabinet.setId(id + "-" + codeCollection.getId());

		Asset asset = convertCode(code, cabinet);
		List<Folder> folders = asset.getFolder();

		Collection<Section> topSections = sectionDao
				.findTopLevelSectionByCode(code.getId());

		for (Section section : topSections) {
			section.setSectionSequence(code.getCodeSequence());
			folders.add(convertSection(section));
		}

		// cabinet.getFolder().add(convertCode(code));

		return asset;
	}

	private Folder convertSection(Section section) {
		Folder folder = new Folder();

		folder.setCategory(section.getLevelType());
		folder.setCategoryIndex(BigInteger.valueOf(section.getSectionSequence()));
		folder.setDisplay(section.getHeading());
		folder.setReference(section.getShortHeading());
		folder.setTitle(section.getTextHeading());
		folder.setBackendID(BigInteger.valueOf(section.getId()));
		folder.setId(section.getCodeId() + "-" + section.getId());

		List<Folder> subfolders = folder.getFolder();
		//Content file = folder.getFile();

		// process content (aka file)
		ContentFull content = contentFullDao.findContentFullBySection(section
				.getId());
		if (content != null) {
			Content jaxbContent = convertFullContent(content);
			if (content.isNotes()) {
				folder.setNotes(jaxbContent);
			} else {
				folder.setFile(jaxbContent);
			}
			// files.add(convertFulLContent(content));
		}

		// recursively process children
		Collection<Section> children = sectionDao.findSectionsByParent(section
				.getId());
		if (children != null) {
			for (Section child : children) {
				subfolders.add(convertSection(child));
			}
		}

		return folder;
	}

	private Cabinet convertCodeCollection(CodeCollection cc) {
		Cabinet cabinet = new Cabinet();
		cabinet.setAbbr(cc.getHeading());
		cabinet.setFederal(cc.isFederal());
		cabinet.setState(cc.isState());
		cabinet.setTitle(cc.getHeading());
		cabinet.setBackendID(BigInteger.valueOf(cc.getId()));
		// cabinet.setId(cc.getId());

		return cabinet;
	}

	private Asset convertCode(Code code, Cabinet cabinet) {

		Asset asset = new Asset();

		asset.setBackendID(BigInteger.valueOf(code.getId()));
		asset.setAbbr(code.getShortHeading());
		asset.setDisplay(code.getHeading());
		asset.setReference(code.getTitle());
		asset.setTitle(code.getName());
		asset.setStatus(code.getStatus());
		
		asset.setCabinet(cabinet);

		/*
		 * List<Folder> subfolders = asset.getFolder();
		 * 
		 * Collection<Section> topSections = sectionDao
		 * .findTopLevelSectionByCode(code.getId()); for (Section section :
		 * topSections) { subfolders.add(convertSection(section)); }
		 */

		return asset;

	}

	private Content convertFullContent(ContentFull content) {
		Content file = new Content();
		file.setId(content.getSectionId() + "-" + content.getId());
		file.setValue(content.getContent());
		file.setContentType(content.getFormatType());
		// file.setIsNotes(content.isNotes());
		return file;
	}

	private Content convertContentChunksContent(
			Collection<ContentPart> contentParts) {
		Content file = new Content();

		// TODO: IMPLEMENT ONCE WE ARE CONFIDENT THE DATA LOOKS GOOD
		//
		// this is the best way to clean up the content (i.e. minimize and
		// normalize html code)
		// also can provide good links here

		return file;
	}
}
