package com.cardence.lawshelf;

import java.util.Collection;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cardence.lawshelf.model.CodeCollection;
import com.cardence.lawshelf.model.CodeCollectionDao;
import com.cardence.lawshelf.model.CodeDao;

@Component
public class UserInteractionGetCollectionIdImpl extends UserInteractionImpl implements UserInteractionGetCollectionId {

	@Autowired
	private Log log;

	@Autowired
	private CodeCollectionDao codeCollectionDao;

	@Autowired
	private CodeDao codeDao;

	/* (non-Javadoc)
	 * @see com.cardence.lawshelf.UserInteractionGetCollectionId#getDesiredCollectionId()
	 */
	public int getDesiredCollectionId() {

		// go and find the collections
		final Collection<CodeCollection> codeCollection = codeCollectionDao.findAllCodeCollections();
		if (codeCollection == null || codeCollection.isEmpty()) {
			String msg = "No Code Collections Found!";
			log.debug(msg);
			System.out.println(msg);
			System.exit(1);
		}

		// create the choices
		UserChoicePresenter selectCollection = new UserChoicePresenter() {
			public void printChoices() {
				for (CodeCollection cc : codeCollection) {
					System.out.println("[" + cc.getId() + "] " + cc.getHeading());
				}
			}
		};

		// ask the question
		String collectionIdStr = super.askQuestion("Select a collection to work with", selectCollection);
		System.out.println("you chose [" + collectionIdStr + "]");

		// massage response
		Integer collectionId = null;
		if (NumberUtils.isNumber(collectionIdStr)) {
			collectionId = NumberUtils.createInteger(collectionIdStr);
		} else {
			System.out.println("Not a valid Collection ID: " + collectionIdStr);
			System.exit(1);
		}

		return collectionId;
	}

}
