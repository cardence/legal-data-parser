package com.cardence.lawshelf;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

import com.cardence.lawshelf.cfr.CfrJaxbToMysqlProcessor;
import com.cardence.lawshelf.cfr.jaxb.CFRDOC;

public class CFRLoaderStartup {

	private static CFRLoaderStartup me;

	/* BEGIN: Spring Configured Variables */

	private String targetPath;

	private String targetTest;

	/* END: Spring Configured Variables */
	
	@Autowired
	private CfrJaxbToMysqlProcessor processor;

	@Autowired
	private Log log;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/app-context.xml");

		me = context.getBean("CFRLoader", CFRLoaderStartup.class);
		Assert.notNull(me, "CFRLoader not created");

		me.run();
	}

	private void executeForTest() throws Exception {
		log.debug("Using target test path: " + targetTest);

		URL fileURL = Thread.currentThread().getContextClassLoader().getResource(targetTest);
		Assert.notNull(fileURL, "File URL Cannot be null");

		File file = new File(fileURL.toURI());
		if (!file.exists()) {
			log.error("ERROR: No file found at " + targetTest);
			return;
		}
		if (!file.canRead()) {
			log.error("ERROR: NOT READABLE at " + targetTest);
			return;
		}

		if (file.isFile()) {
			log.info("Target is a file... Processing file: " + file.getName());
			processFile(file);
		} else {
			log.error("Could not determine if the target is a file or a directory... Exiting");
		}

	}

	private void processFileRecursively(File file) throws Exception {
		if (!file.exists()) {
			log.error("ERROR: No file found at " + targetPath);
			return;
		}
		if (!file.canRead()) {
			log.error("ERROR: NOT READABLE at " + targetPath);
			return;
		}
		if (file.isDirectory()) {
			log.info("Target is directory... Proceeding to iterate through each file");
			File[] filearray = file.listFiles();
			for (File f : filearray) {
				processFileRecursively(f);
			}
		} else if (file.isFile()) {
			log.info("Target is a file... Processing file: " + file.getName());
			String name = file.getName();
			if (name.endsWith(".xml") || name.endsWith(".XML")) {
				log.info("Processing file: " + name);
				processFile(file);
			} else {
				log.warn("Found file [" + name + "] that does not appear to be an xml file... SKIPPING");
			}
		} else {
			log.error("Could not determine if the target is a file or a directory... Exiting");
		}

	}

	private void executeForProduction() throws Exception {
		log.debug("Using target path: " + targetPath);
		File file = new File(targetPath);
		processFileRecursively(file);
	}

	public void run() {
		Assert.notNull(log, "Log not created");
		Assert.state(targetPath != null || targetTest != null, "Target path or test is Null");

		try {
			if (targetTest != null) {
				executeForTest();
			} else if (targetPath != null) {
				executeForProduction();
			}

		} catch (Exception e) {
			log.error("Caught an error", e);
		}
	}

	private void processFile(File file) throws IOException, JAXBException {

		JAXBContext jc = JAXBContext.newInstance("com.cardence.lawshelf.cfr.jaxb");
		Unmarshaller u = jc.createUnmarshaller();
		Object o = u.unmarshal(file);
		
		String filename = file.getName();
		filename = StringUtils.substringAfter(filename, "-");
		filename = StringUtils.substringBefore(filename, "-");
		if (!NumberUtils.isNumber(filename)) {
			throw new RuntimeException("Was expecting a year for the collection. instead got: " + filename);
		}
		
		if (o instanceof CFRDOC) {
			this.processor.processCfrResource((CFRDOC) o, NumberUtils.createInteger(filename));
			//System.exit(0);
		}

		System.out.println(ObjectUtils.toString(o));

	}

	public String getTargetPath() {
		return targetPath;
	}

	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}

	public String getTargetTest() {
		return targetTest;
	}

	public void setTargetTest(String targetTest) {
		this.targetTest = targetTest;
	}

}
