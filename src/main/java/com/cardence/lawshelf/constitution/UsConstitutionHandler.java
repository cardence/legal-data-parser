package com.cardence.lawshelf.constitution;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cardence.lawshelf.model.Code;
import com.cardence.lawshelf.model.CodeCollection;
import com.cardence.lawshelf.model.CodeCollectionDao;
import com.cardence.lawshelf.model.CodeDao;
import com.cardence.lawshelf.model.ContentFull;
import com.cardence.lawshelf.model.ContentFullDao;
import com.cardence.lawshelf.model.ContentPart;
import com.cardence.lawshelf.model.ContentPartDao;
import com.cardence.lawshelf.model.Section;
import com.cardence.lawshelf.model.SectionDao;

@Component
public class UsConstitutionHandler {

	@Autowired
	private Log log;

	@Autowired
	private CodeCollectionDao codeCollectionDao;

	@Autowired
	private CodeDao codeDao;

	@Autowired
	private SectionDao sectionDao;

	@Autowired
	private ContentFullDao contentFullDao;

	@Autowired
	private ContentPartDao contentPartDao;

	// private final com.cardence.lawshelf.model.xml.Code xmlCode;

	public UsConstitutionHandler() {
		super();
	}

	public void convertXmlToMySql(
			final com.cardence.lawshelf.model.xml.Code xmlCode) {

		Code mysqlCode = convertCodeXml(xmlCode);

		com.cardence.lawshelf.model.xml.CodeCollection xmlCollection = xmlCode
				.getCodeCollection();

		CodeCollection mysqlCollection = convertCodeCollectionXml(xmlCollection);
		this.storeCodeCollection(mysqlCollection);

		mysqlCode.setCodeCollection(mysqlCollection);
		mysqlCode.setCodeCollectionId(mysqlCollection.getId());

		this.storeCode(mysqlCode);

		List<com.cardence.lawshelf.model.xml.Section> sectionList = xmlCode
				.getSection();
		if (sectionList != null) {
			for (com.cardence.lawshelf.model.xml.Section section : sectionList) {
				this.convertSectionXml(section, null, mysqlCode);
			}
		}

	}

	private Code convertCodeXml(com.cardence.lawshelf.model.xml.Code xmlCode) {
		Code c = new Code();
		c.setTitle(xmlCode.getTitle());
		c.setStatus(xmlCode.getStatus());
		c.setShortHeading(xmlCode.getShortHeading());
		c.setName(xmlCode.getTitle());
		c.setHeading(xmlCode.getHeading());
		if (xmlCode.getSequence() != null) {
			c.setCodeSequence(xmlCode.getSequence().intValue());
		}

		return c;
	}

	private CodeCollection convertCodeCollectionXml(
			com.cardence.lawshelf.model.xml.CodeCollection xmlCollection) {
		CodeCollection cc = new CodeCollection();
		cc.setFederal(xmlCollection.isFederal());
		cc.setStateCode(xmlCollection.getStateCode());
		cc.setHeading(xmlCollection.getHeading());
		cc.setName(xmlCollection.getTitle());
		if (xmlCollection.getYear() != null) {
			cc.setYear(Integer.valueOf(xmlCollection.getYear().intValue()));
		}
		return cc;
	}

	private Section convertSectionXml(
			com.cardence.lawshelf.model.xml.Section xmlSection, Section parent,
			Code code) {
		Section s = new Section();
		s.setHeading(xmlSection.getHeading());
		s.setTextHeading(xmlSection.getTitle());
		s.setLevelPosition("" + xmlSection.getLevelPosition());
		s.setLevelType(xmlSection.getLevelType());
		if (xmlSection.getSequence() != null) {
			s.setSectionSequence(xmlSection.getSequence().intValue());
		}
		s.setSourceReference(xmlSection.getReference());
		s.setShortHeading(xmlSection.getShortHeading());

		// set in the code
		s.setCode(code);
		s.setCodeId(code.getId());

		// set in the parent
		if (parent != null) {
			s.setParent(parent);
			s.setParentSectionId(parent.getId());
		}

		// store!
		this.storeSection(s);

		// recursion: now process all the child sections
		List<com.cardence.lawshelf.model.xml.Section> children = xmlSection
				.getSection();
		if (children != null) {
			for (com.cardence.lawshelf.model.xml.Section child : children) {
				this.convertSectionXml(child, s, code);
			}
		}

		// children are done... do the notes
		if (xmlSection.getNotes() != null) {
			ContentFull notes = convertContentFullXml(xmlSection.getNotes(), s);
			this.storeContentFull(notes, true);
		}
		// children are done... do the content
		if (xmlSection.getFile() != null) {
			ContentFull file = convertContentFullXml(xmlSection.getFile(), s);
			this.storeContentFull(file, true);
		}

		return s;
	}

	private ContentFull convertContentFullXml(
			com.cardence.lawshelf.model.xml.Content xmlContent, Section section) {
		ContentFull c = new ContentFull();
		c.setFormatType(xmlContent.getContentType());
		c.setContent(xmlContent.getValue());
		c.setNotes(xmlContent.isIsNotes());

		c.setSection(section);
		c.setSectionId(section.getId());

		return c;
	}

	/* ****
	 * 
	 * STORE HANDLER METHODS
	 * 
	 * ***
	 */
	private void storeCodeCollection(CodeCollection codeCollection) {
		assert codeCollection != null : "code collection is null";
		boolean isUpdate = true;

		if (codeCollection.getId() == null) {
			Collection<CodeCollection> foundCodeCollections = this.codeCollectionDao
					.findCodeCollectionsByNameAndYear(codeCollection.getName(),
							codeCollection.getYear());
			if (foundCodeCollections != null && !foundCodeCollections.isEmpty()) {
				// found one... update
				assert foundCodeCollections.size() == 1;
				CodeCollection foundCodeCol = (CodeCollection) foundCodeCollections
						.toArray()[0];
				codeCollection.setId(foundCodeCol.getId());
				this.codeCollectionDao.updateCodeCollection(codeCollection);
			} else {
				// did not find one... create
				Integer newId = this.codeCollectionDao
						.createCodeCollection(codeCollection);
				isUpdate = false;
				codeCollection.setId(newId);
			}
		} else {
			this.codeCollectionDao.updateCodeCollection(codeCollection);
		}

		// now store any attributes
		// this.storeAttributes(codeCollection.getAttributes(), isUpdate);
	}

	private void storeCode(Code code) {
		assert code != null : "code is null";

		boolean isUpdate = true;

		if (code.getId() == null) {
			Collection<Code> foundCodes = this.codeDao
					.findCodeByNameAndSequenceAndCollection(code.getName(),
							code.getCodeSequence(), code.getCodeCollectionId());

			if (foundCodes != null && !foundCodes.isEmpty()) {
				// found one... update
				assert foundCodes.size() == 1;
				Code foundCode = (Code) foundCodes.toArray()[0];
				code.setId(foundCode.getId());
				this.codeDao.updateCode(code);
			} else {
				// did not find one... create
				Integer newId = this.codeDao.createCode(code);
				isUpdate = false;
				code.setId(newId);
			}
		} else {
			this.codeDao.updateCode(code);
		}

		log.info("");
		if (isUpdate) {
			log.info("... updating code [" + code.getHeading() + "]");
		} else {
			log.info("NEW CODE RECORD: " + code.getHeading());
		}
		log.info("");

		// now take care of the aliases
		// this.storeCodeAlias(code);

		// now store any attributes
		// this.storeAttributes(code.getAttributes(), isUpdate);
	}

	private void storeSection(Section section) {
		assert section != null : "section is null";

		if (section.getId() == null) {
			Section foundSection = this.sectionDao
					.findSectionByCodeAndReference(section.getCodeId(),
							section.getSourceReference());

			if (foundSection != null) {
				// found one... update
				section.setId(foundSection.getId());
				this.sectionDao.updateSection(section);
				section.setIsNewRecord(Boolean.FALSE);
			} else {
				// did not find one... create
				Integer newId = this.sectionDao.createSection(section);
				section.setIsNewRecord(Boolean.TRUE);
				section.setId(newId);
			}
		} else {
			this.sectionDao.updateSection(section);
			section.setIsNewRecord(Boolean.FALSE);
		}

		if (section.getIsNewRecord()) {
			log.info("SECTION CREATED: " + section.getSourceReference());
			log.info("   --> heading: " + section.getHeading());
		} else {
			log.info(" ... updating section [" + section.getHeading() + "]");
		}

		// now store any attributes
		// this.storeAttributes(section.getAttributes(),
		// !section.getIsNewRecord());
	}

	private void storeContentFull(ContentFull content, boolean isDeleteFirst) {
		assert content != null : "content full is null";
		boolean isUpdate = true;

		if (isDeleteFirst) {
			this.contentFullDao.deleteAllForSection(content.getSectionId());
		}

		if (content.getId() == null) {
			Integer newId = this.contentFullDao.createContentFull(content);
			content.setId(newId);
			isUpdate = false;
		} else {
			this.contentFullDao.updateContentFull(content);
		}

		// now store any attributes
		// this.storeAttributes(content.getAttributes(), isUpdate);
	}

	private void storeContentPart(ContentPart content) {
		assert content != null : "content part is null";
		boolean isUpdate = true;

		if (content.getId() == null) {
			Integer newId = this.contentPartDao.createContentPart(content);
			isUpdate = false;
			content.setId(newId);
		} else {
			this.contentPartDao.updateContentPart(content);
		}

		// now store any attributes
		// this.storeAttributes(content.getAttributes(), isUpdate);
	}

	private void storeContentParts(int sectionId, int contentFullId,
			Collection<ContentPart> contents, boolean isDeleteFirst) {
		if (isDeleteFirst) {
			this.contentPartDao.deleteAllAttributesForSection(sectionId);
			this.contentPartDao.deleteAllForSection(sectionId);
		}

		for (ContentPart part : contents) {
			part.setContentId(contentFullId);
			storeContentPart(part);
		}
	}

}
