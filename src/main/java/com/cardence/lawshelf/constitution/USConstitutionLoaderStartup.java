package com.cardence.lawshelf.constitution;

import java.io.File;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;
import org.xml.sax.SAXException;

import com.cardence.lawshelf.model.xml.Code;
import com.cardence.lawshelf.model.xml.CodeCollection;
import com.cardence.lawshelf.model.xml.Content;
import com.cardence.lawshelf.model.xml.Section;

public class USConstitutionLoaderStartup {

	private static USConstitutionLoaderStartup me;

	@Autowired
	private UsConstitutionHandler handler;

	private String targetPath;

	private String xsdFileProperty;

	@Autowired
	private Log log;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"META-INF/spring/app-context.xml");

		me = context.getBean("USConstitutionLoader",
				USConstitutionLoaderStartup.class);
		Assert.notNull(me, "USConstitutionLoader not created");

		me.run();
	}

	private void execute() throws Exception {
		log.debug("Using target path: " + targetPath);

		File file = new File(targetPath);
		if (!file.exists()) {
			log.error("ERROR: No file found at " + targetPath);
			return;
		}
		if (!file.canRead()) {
			log.error("ERROR: NOT READABLE at " + targetPath);
			return;
		}
		if (file.isDirectory()) {
			log.info("Target is directory... Proceeding to iterate through each file");
			File[] filearray = file.listFiles();
			log.info("... Directory contains "
					+ ((filearray == null) ? 0 : filearray.length) + " files");
			for (File f : filearray) {
				String name = f.getName();
				if (name.endsWith(".xml") || name.endsWith(".XML")) {
					log.info("Processing file: " + f.getName());
					processFile(f);
				}
			}
		} else if (file.isFile()) {
			log.info("Target is a file... Processing file: " + file.getName());
			processFile(file);
		} else {
			log.error("Could not determine if the target is a file or a directory... Exiting");
		}
	}

	private void processFile(File f) {

		URL xsdFile = Thread.currentThread().getContextClassLoader()
				.getResource(xsdFileProperty);
		try {
			Class[] classList = new Class[] { Code.class, CodeCollection.class,
					Content.class, Section.class };
			JAXBContext context = JAXBContext.newInstance(classList);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			// validate
			SchemaFactory schemaFactory = SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(xsdFile);
			unmarshaller.setSchema(schema);

			Code code = (Code) unmarshaller.unmarshal(f);
			Assert.notNull(code, "Could not convert xml to code");

			handler.convertXmlToMySql(code);
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		Assert.notNull(log, "Log not created");
		Assert.notNull(targetPath, "Target path property is NULL");
		Assert.notNull(xsdFileProperty, "XSD file property is NULL");

		Assert.state(Thread.currentThread().getContextClassLoader()
				.getResource(xsdFileProperty) != null,
				"Could not find XSD FILE RESOURCE");

		try {
			execute();
		} catch (Exception e) {
			log.error("Caught an error", e);
		}
	}

	public String getTargetPath() {
		return targetPath;
	}

	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}

	public String getXsdFileProperty() {
		return xsdFileProperty;
	}

	public void setXsdFileProperty(String xsdFileProperty) {
		this.xsdFileProperty = xsdFileProperty;
	}

}
